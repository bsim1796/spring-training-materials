package edu.geography;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class PopulationProcessor {

    private List<Country> countries = new ArrayList<>();
    private List<City> cities = new ArrayList<>();
    private static final String CITY = "City";
    private static final String COUNTRY = "Country";
    private static final String OBJECT_TYPE = "Type";
    private static final String OBJECT_NAME = "Name";
    private static final String OBJECT_POPULATION = "Population";
    private static final String OBJECT_COUNTRY = "Country";
    private static final String OBJECT_AREA = "Area";
    private static final String OBJECT_NUMBER_OF_CITIES = "NumberOfCities";
    private static final String OBJECT_POPULATION_DENSITY = "PopulationDensity";
    private static ArrayList<String> header;

    public void read() {
        //read countries
        try(BufferedReader reader = new BufferedReader(
                new FileReader("src/edu/geography/resources/places.txt"))) {
            //read the header
            String headers = reader.readLine();
            header= new ArrayList<>(Arrays.asList(headers.split(" ")));

            String line = reader.readLine();
            while (line != null) {
                String[] data = line.split(" ");
                if (COUNTRY.equals(data[header.indexOf(OBJECT_TYPE)])) {
                        countries.add(new Country(
                                data[header.indexOf(OBJECT_NAME)],
                                Integer.parseInt(data[header.indexOf(OBJECT_POPULATION)]),
                                Double.parseDouble(data[header.indexOf(OBJECT_AREA)])));
                }
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            System.out.println("The file not found");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IO exception occurred");
            e.printStackTrace();
        }
        try(BufferedReader reader = new BufferedReader(
                new FileReader("src/edu/geography/resources/places.txt"))) {
            reader.readLine(); //ignoring header this time
            String line = reader.readLine();
            while (line != null) {
                String[] data = line.split(" ");
                if(CITY.equals(data[header.indexOf(OBJECT_TYPE)])) {
                    for (Country country :countries) {
                        if(Objects.equals(country.getName(), data[header.indexOf(OBJECT_COUNTRY)])) {
                            City city = new City(
                                    data[header.indexOf(OBJECT_NAME)],
                                    Integer.parseInt(data[header.indexOf(OBJECT_POPULATION)]));
                            country.addCity(city);
                            cities.add(city);
                        }
                    }
                }
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            System.out.println("The file not found");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IO exception occurred");
            e.printStackTrace();
        }
    }

    public void sortCountriesByCityNumber(){
        List<Country> result = countries.stream().sorted(Comparator.comparingInt(o -> o.getCities().size()))
                .collect(Collectors.toList());
        try {
            FileWriter fileWriter = new FileWriter("src/edu/geography/resources/countrySortedByCities.txt");
            if (header!=null) {
                fileWriter.write(OBJECT_NAME + " " + OBJECT_POPULATION + " " + OBJECT_AREA + " " + OBJECT_NUMBER_OF_CITIES + "\n");
            }
            for (Country country :result) {
                fileWriter.write(country.getName() + " " + country.getPopulation() + " " + country.getSurfaceArea()
                        + " " + country.getCities().size() + "\n");
            }
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public void sortCountriesByDensity() {
        List<Country> result = countries.stream().filter(o -> o.getPopulation()!=0)
                .sorted(Comparator.comparingDouble(o -> o.getPopulation()/o.getSurfaceArea()))
                .collect(Collectors.toList());
        try {
            FileWriter fileWriter = new FileWriter("src/edu/geography/resources/countrySortedByDensity.txt");
            if (header!=null) {
                fileWriter.write(OBJECT_NAME + " " + OBJECT_POPULATION + " " + OBJECT_AREA + " " + OBJECT_POPULATION_DENSITY + "\n");
            }
            for (Country country :result) {
                fileWriter.write(country.getName() + " " + country.getPopulation() + " " + country.getSurfaceArea()
                        + " " + country.getPopulation()/country.getSurfaceArea() +"\n");
            }
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public void sortCitiesByPopulation() {
        List<City> result = cities.stream().sorted(Comparator.comparingDouble(City::getPopulation).reversed())
                .collect(Collectors.toList());
        try {
            FileWriter fileWriter = new FileWriter("src/edu/geography/resources/citySortedByPopulation.txt");
            if (header!=null) {
                fileWriter.write(OBJECT_NAME + " " + OBJECT_POPULATION + "\n");
            }
            for (City city :result) {
                fileWriter.write(city.getName() + " " + city.getPopulation() + "\n");
            }
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
