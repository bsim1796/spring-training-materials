package edu.geography;

import java.util.ArrayList;
import java.util.List;

public class Country {
        private String name;
        private double surfaceArea;
        private int population;
        private List<City> cities = new ArrayList<>();

        public Country() {
        }

        public Country(String name, int population, double surfaceArea) {
                this.name = name;
                this.surfaceArea = surfaceArea;
                this.population = population;
        }

        public void addCity(City city){
                cities.add(city);
        }

        @Override
        public String toString() {
                return "Country [ name=" + name + ", population=" + population + "]";
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public double getSurfaceArea() {
                return surfaceArea;
        }

        public void setSurfaceArea(double surfaceArea) {
                this.surfaceArea = surfaceArea;
        }

        public int getPopulation() {
                return population;
        }

        public void setPopulation(int population) {
                this.population = population;
        }

        public List<City> getCities() {
                return cities;
        }

        public void setCities(List<City> cities) {
                this.cities = cities;
        }
}

