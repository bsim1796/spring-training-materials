package edu.geography;

public class Main {
    public static void main(String[] args) {
        PopulationProcessor populationProcessor = new PopulationProcessor();

        populationProcessor.read();
        populationProcessor.sortCountriesByCityNumber();
        populationProcessor.sortCountriesByDensity();
        populationProcessor.sortCitiesByPopulation();
    }
}
