import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.TestInstance;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UnitTests {
    private static final Logger LOGGER = LogManager.getLogger(UnitTests.class);
    private static ShoppingCart shoppingCart;
    private static Product product;

    @Before
    public void init() {
        shoppingCart = new ShoppingCart();
        product = new Product("tv",444.65);
    }

    @BeforeAll
    public void setup() {
        LOGGER.info("Start testing");

    }

    @AfterEach
    public void tearDown() {
        LOGGER.info("This test is done");
    }

    @Test
    public void initializedRight() {
        assertEquals(0,shoppingCart.getItemCount());
    }

    @Test
    public void isCartEmpty() {
        shoppingCart.addItem(product);
        try {
            shoppingCart.removeItem(product);
        } catch (ProductNotFoundException e) {
            e.printStackTrace();
        }
        assertEquals(0, shoppingCart.getItemCount());
    }

    @Test
    public void cartCountIncrementingProperly() {
        int itemCountBefore = shoppingCart.getItemCount();
        shoppingCart.addItem(product);
        assertEquals(itemCountBefore+1,shoppingCart.getItemCount());
    }

    @Test
    public void cartCountDecreasedProperly() {
        shoppingCart.addItem(product);
        int itemCountBefore = shoppingCart.getItemCount();
        try {
            shoppingCart.removeItem(product);
        } catch (ProductNotFoundException e) {
            e.printStackTrace();
        }
        assertEquals(itemCountBefore-1,shoppingCart.getItemCount());
    }

    @Test
    public void cartBalanceIncrementingProperly() {
        double cartBalanceBefore = shoppingCart.getBalance();
        shoppingCart.addItem(product);
        assertEquals(cartBalanceBefore+product.getPrice(),shoppingCart.getBalance(),0.000001);
    }

    @Test
    @DisplayName("Check if an exception is thrown when a product that is not in cart is removed")
    public void itemNotInCartRemovedThenExceptionThrown() {
        ProductNotFoundException exception = assertThrows(ProductNotFoundException.class, () -> {
            shoppingCart.removeItem(product);
        });
        assertNotNull(exception);
    }
}
