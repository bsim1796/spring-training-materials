import java.util.HashMap;
import java.util.Map;

public class SugarCoffeeMachine extends CoffeeMachine{

    SugarCoffeeMachine(SimpleCoffee coffee){
        super(coffee,1,"Sugar");
    }

}
