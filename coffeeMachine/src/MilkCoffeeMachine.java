import java.util.HashMap;
import java.util.Map;

public class MilkCoffeeMachine extends CoffeeMachine{

    MilkCoffeeMachine(SimpleCoffee coffee){
        super(coffee,2,"Milk");
    }
}
