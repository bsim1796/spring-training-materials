import java.util.*;

public class MixedCoffee implements SimpleCoffee {

    private Integer cost;
    private String ingredients  = "";

    MixedCoffee() {
        this.cost=5;
        this.ingredients = "Black Coffee" ;
    }

    MixedCoffee(int cost, String ingredients) {
        this.cost=cost;
        this.ingredients = ingredients ;
    }

    @Override
    public int getCost() {
        return cost;
    }

    @Override
    public String getIngredients() {
        return ingredients;
    }

    @Override
    public void addIngredient(int cost, String label) {
            this.ingredients = this.ingredients + ", " + label;
            this.cost += cost;
    }
}
