import java.util.HashMap;
import java.util.Map;

public class CreamCoffeeMachine extends CoffeeMachine{

    CreamCoffeeMachine (SimpleCoffee coffee) {
        super(coffee,6,"Cream");
    }
}
