import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class CoffeeMachine {

    private SimpleCoffee coffee;

    CoffeeMachine(SimpleCoffee coffee){
        this.coffee = coffee;
    }

    CoffeeMachine(SimpleCoffee coffee, Integer cost, String ingredient) {
        this.coffee = coffee;
        coffee.addIngredient(cost, ingredient);

    }

    public SimpleCoffee getCoffee() {
        return coffee;
    }

    public int getCost() {
        return coffee.getCost();
    }

    public String getIngredients() {
        return coffee.getIngredients();
    }
}
