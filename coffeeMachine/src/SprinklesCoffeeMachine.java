import java.util.HashMap;
import java.util.Map;

public class SprinklesCoffeeMachine extends CoffeeMachine {

    SprinklesCoffeeMachine(SimpleCoffee coffee) {
        super(coffee, 4, "Sprinkles");
    }
}
