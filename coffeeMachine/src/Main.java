public class Main {

    public static void main(String[] args) {

        SimpleCoffee coffee = new MixedCoffee(5, "Black Coffee");
        CoffeeMachine coffeeMachine = new CoffeeMachine(coffee);
        coffeeMachine = new SprinklesCoffeeMachine(coffeeMachine.getCoffee());
        coffeeMachine = new MilkCoffeeMachine(coffeeMachine.getCoffee());
        coffeeMachine = new SugarCoffeeMachine(coffeeMachine.getCoffee());
        coffeeMachine = new MilkCoffeeMachine(coffeeMachine.getCoffee());
        coffeeMachine = new CreamCoffeeMachine(coffeeMachine.getCoffee());

        System.out.println("Final price: " + coffeeMachine.getCost());
        System.out.println("List Of ingredients: " + coffeeMachine.getIngredients());
    }
}
