import java.util.Map;

public interface SimpleCoffee {
    int getCost();
    String getIngredients();
    void addIngredient (int cost, String label);
}
