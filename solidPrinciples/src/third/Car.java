package third;

public class Car {
    Engine engine;

    public Car (Engine engine) {
        this.engine = engine;
    }

    /**
     * There was violated the dependency inversion principle. The class Car was depend the concrete class Engine.
     * If we use an engine interface, the car will work with any type of engine, but there must be one.
     */
    public void start () {
        engine.start();
    }
}
