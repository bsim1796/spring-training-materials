package fourth;

public class Book {

    public String author;
    public String title;

    /**
     * The single responsibility principle was violated here. The save method did a lot of business logic too, instead of just
     * saving the book. Creating atomic methods with only one purpose resolves the problem.
     */

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
