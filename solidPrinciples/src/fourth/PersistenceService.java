public class PersistenceService {
    
    public static void save() {
        //save the book
    }

    public static void openConnection () {
        //open connection
    }

    public static void check() {
        // make logic checks
    }

    public static void checkIfSomething() {
        // CRUD operations
    }

    public static void closeConnection() {
        //close connection
    }
}
