package second;

public class Bike implements Vechile{

    /**
     * The interface segregation SOLID Principle was violated, because the class Bike was forced to implement a method that are
     * not needed for him. With creating a new interface VehicleWithDoors what extends the Vehicle, we can resolve the issue.
     */
    @Override
    public void drive() {
        System.out.println(this.getClass().getSimpleName() + " driven");
    }

    @Override
    public void stop() {
        System.out.println(this.getClass().getSimpleName() + " stopped");
    }

    @Override
    public void refuel() {
        System.out.println(this.getClass().getSimpleName() + " refueled");
    }
}
