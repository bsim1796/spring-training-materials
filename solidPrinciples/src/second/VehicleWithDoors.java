package second;

public interface VehicleWithDoors{
    public void drive();
    public void stop();
    public void refuel();
    public void openDoors();
}
