package first;

public class Truck extends Vehicle{

    public Truck(double value) {
        super(value);
    }

    @Override
    public double calculateValue() {
        return getValue() * 1.4;
    }

}
