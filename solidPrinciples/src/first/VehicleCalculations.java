package first;

public class VehicleCalculations {

    /**
     * The old class violated the Open/closed SOLID Principle, because as we wanted to introduce a new data type,
     * the program was not opened for extension, and modifications were required in the source code.
     * The best way to deal with this problem, is to create an interface, so every class could implement the
     * calculations as they want. Also, thanks to the polymorphism, we can refer to each object as a Vehicle.
    */

    public double calculateValue(Vehicle vehicle) {
        return vehicle.calculateValue();
    }
}
