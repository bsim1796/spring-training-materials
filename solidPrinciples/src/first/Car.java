package first;

public class Car extends Vehicle{

    Car(double value) {
        super(value);
    }

    @Override
    public double calculateValue() {
        return getValue() * 0.8;
    }

}
