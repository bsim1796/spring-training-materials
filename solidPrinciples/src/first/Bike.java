package first;

public class Bike extends Vehicle{


    Bike(double value) {
        super(value);
    }

    @Override
    public double calculateValue() {
        return getValue()* 0.5;
    }

}
