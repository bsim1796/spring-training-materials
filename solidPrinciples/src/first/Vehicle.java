package first;

public abstract class Vehicle {

    private double value;

    public Vehicle(double value) {
        this.value = value;
    }

    public double getValue() {
        return this.value;
    }

    abstract double calculateValue();
}
