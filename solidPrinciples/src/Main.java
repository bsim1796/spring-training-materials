import first.Truck;
import first.Vehicle;
import first.VehicleCalculations;
import second.Bike;
import second.Vechile;
import third.Car;
import third.Engine;
import third.PetrolEngine;

public class Main {


    public static void main(String[] args) {
        Vehicle truck = new Truck(4);
        VehicleCalculations vehicleCalculations = new VehicleCalculations();
        System.out.println(vehicleCalculations.calculateValue(truck));

        Vechile bike = new Bike();
        bike.drive();
        bike.stop();
        bike.refuel();

        Engine engine = new PetrolEngine();
        Car car = new Car(engine);
        car.start();


    }
}
