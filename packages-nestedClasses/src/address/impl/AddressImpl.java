package address.impl;

import address.Address;
import enterprise.impl.EnterpriseImpl;
import enterprise.Enterprise;

public class AddressImpl implements Address {
    private String street;
    private String city;
    private String country;
    private String state;
    private int postalCode;

    public AddressImpl(String street, String city, String country, String state, int postalCode) {
        this.street = street;
        this.city = city;
        this.country = country;
        this.state = state;
        this.postalCode = postalCode;
    }

    public AddressImpl(Address address){
        this.street = address.getStreet();
        this.city = address.getCity();
        this.country = address.getCountry();
        this.state = address.getState();
        this.postalCode = address.getPostalCode();
    }

    public static String informationMessage (EnterpriseImpl.Employee employee, Enterprise.Employee.LockerAddress locker, Address.PackageDelivery delivery) {
        final StringBuffer sb = new StringBuffer("Dear ");
        sb.append(employee.getName()).append(" from ").append(employee.getEnterprise().getTitle()).append("!\n");
        sb.append("Your package with a weight of ").append(delivery.getWeight());
        sb.append(" will arrive soon to the locker:\n").append(locker.getLockerAddress());
        return sb.append("\nBest regards!").toString();
    }

    public static class PackageDelivery implements Address.PackageDelivery{
        private int weight;
        private Address address;

        public PackageDelivery(int weight, Address address) {
            this.weight = weight;
            this.address = address;
        }

        public int getWeight() {
            return weight;
        }

        public Address getAddress() {
            return address;
        }
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getState() {
        return state;
    }

    public int getPostalCode() {
        return postalCode;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("address.Address{");
        sb.append("street='").append(street).append('\'');
        sb.append(", city='").append(city).append('\'');
        sb.append(", country='").append(country).append('\'');
        sb.append(", state='").append(state).append('\'');
        sb.append(", postalCode=").append(postalCode);
        sb.append('}');
        return sb.toString();
    }
}
