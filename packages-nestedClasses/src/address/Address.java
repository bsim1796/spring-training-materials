package address;

public interface Address {

    String getStreet();

    String getCity();

    String getCountry();

    public String getState();

    int getPostalCode();


    interface PackageDelivery {
        int getWeight();

        Address getAddress();
    }
}
