import address.impl.AddressImpl;
import address.Address;
import enterprise.impl.EnterpriseImpl;
import enterprise.Enterprise;

public class Main {

    public static void main(String[] args) {
        Address businessAddress = new AddressImpl("Park Avenue","Manhattan", "USA", "New York", 12345);
        Address personalAddress = new AddressImpl("Main road", "Blackberry", "USA", "Ohio", 54321);
        Address lockerAddress = new AddressImpl("Hollow road", "Blackberry", "USA", "Ohio", 54321);
        Enterprise aaahaEnterprise = new EnterpriseImpl("Aaaha TM.",businessAddress);
        Enterprise.Employee budMud = ((EnterpriseImpl) aaahaEnterprise).new EmployeeImpl("485746581","Bud Mud",personalAddress);
        Enterprise.Employee.LockerAddress locker = ((EnterpriseImpl.EmployeeImpl) budMud).new LockerAddressImpl((AddressImpl) lockerAddress,63046);
        Address.PackageDelivery delivery = new AddressImpl.PackageDelivery(99,personalAddress);

        System.out.println(AddressImpl.informationMessage(budMud, locker, delivery));
    }
}
