package enterprise.impl;

import address.impl.AddressImpl;
import address.Address;
import enterprise.Enterprise;

public class EnterpriseImpl implements Enterprise {
    private String title;
    private Address address;

    public EnterpriseImpl(String title, Address address) {
        this.title = title;
        this.address = address;
    }

    public class EmployeeImpl implements Enterprise.Employee {
        private String employeeNumber;
        private String name;
        private Address personalAddress;
        private Address officeAddress;
        private Enterprise enterprise;

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Employee{");
            sb.append("employeeNumber='").append(employeeNumber).append('\'');
            sb.append(", name='").append(name).append('\'');
            sb.append(", personalAddress=").append(personalAddress);
            sb.append(", officeAddress=").append(officeAddress);
            sb.append(", enterprise=").append(enterprise);
            sb.append('}');
            return sb.toString();
        }

        public EmployeeImpl(String employeeNumber, String name, Address personalAddress) {
            this.employeeNumber = employeeNumber;
            this.name = name;
            this.personalAddress = personalAddress;
            this.officeAddress = EnterpriseImpl.this.getAddress();
            this.enterprise = EnterpriseImpl.this;
        }

        public class LockerAddressImpl extends AddressImpl implements Employee.LockerAddress {
            private int lockerNumber;

            public LockerAddressImpl(AddressImpl address, int lockerNumber) {
                super(address);
                this.lockerNumber = lockerNumber;
            }

            public String getLockerAddress () {
                final StringBuffer sb = new StringBuffer("");
                sb.append(EnterpriseImpl.EmployeeImpl.this.toString())
                        .append("\n")
                        .append(this.toString());
                return sb.toString();
            }

        }

        public String getName() {
            return name;
        }

        public Enterprise getEnterprise() {
            return enterprise;
        }
    }


    public String getTitle() {
        return title;
    }

    public Address getAddress() {
        return address;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("enterprise.Enterprise{");
        sb.append("title='").append(title).append('\'');
        sb.append(", address=").append(address);
        sb.append('}');
        return sb.toString();
    }
}
