package enterprise;

import address.Address;

public interface Enterprise {

    String getTitle();

    Address getAddress();

    interface Employee {

        String getName();

        Enterprise getEnterprise();


        interface LockerAddress extends Address{
            String getLockerAddress ();
        }
    }
}
