import java.util.ArrayList;

public class Library<T extends MediaProduct> {
    private ArrayList<T> stock;

    public Library() {
        stock = new ArrayList<>();
    }

    public void add(T product) {
        stock.add(product);
    }

    public ArrayList<T> getStock() {
        return stock;
    }

    public ArrayList<T> filterByCategory(String category) {
        ArrayList<T> productsByCategory = new ArrayList<>();
        if(category != null) {
            for (T product:stock) {
                if(category.equals(product.getCategory())) {
                    productsByCategory.add(product);
                    System.out.println(product.getTitle());
                }
            }
        } 
        return productsByCategory;
    }

}
