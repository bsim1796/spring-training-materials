public abstract class MediaProduct {
    private final String title;
    private final String mediaURL;
    private final String category;

    public MediaProduct(String title, String mediaURL, String category) {
        this.title = title;
        this.mediaURL = mediaURL;
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public String getMediaURL() {
        return mediaURL;
    }

    public String getCategory() {
        return category;
    }
}
