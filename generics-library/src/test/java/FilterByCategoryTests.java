import org.junit.Test;
import org.junit.jupiter.api.*;

import javax.print.attribute.standard.Media;

import static org.junit.Assert.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FilterByCategoryTests<T extends MediaProduct> {
    private Library<MediaProduct> library = new Library<>();;

    @Test
    public void ifEmpty() {
        assertEquals(0,library.getStock().size());
    }

    @Test
    public void ifNoCategory() {
        Book book = new Book("Divergent", "asdf233dsd", "Fantasy");
        Video video = new Video("Best fails 2020", "wwdxs334!3ef", "Funny");
        library.add(book);
        library.add(video);
        assertEquals(0,library.filterByCategory("History").size());
    }

    @Test
    public void ifTheresACategory() {
        Book book = new Book("Divergent", "asdf233dsd", "Fantasy");
        Video video = new Video("Best fails 2020", "wwdxs334!3ef", "Funny");
        Newspaper newspaper = new Newspaper("Chicaco 1988", "q3eewq==23a", "History");
        library.add((T) book);
        library.add((T) video);
        library.add((T) newspaper);
        assertEquals(1,library.filterByCategory("History").size());
    }
}
