import java.util.Random;

public class Main {

    private static final Day.Weekday[] weekdays = Day.Weekday.values();

    private static void printLogicalRelation(int a, int b ) {
        int comparison = weekdays[a].compareTo(weekdays[b]);

        if ( comparison == 0) {
            System.out.println("Both days are the same: " + weekdays[a].getEnglishName());
        }
        else if ( comparison < 1 ) {
            System.out.println(weekdays[a].getEnglishName() + " is earlier than " + weekdays[b].getEnglishName());
        }
        else {
            System.out.println(weekdays[a].getEnglishName() + " is later than " + weekdays[b].getEnglishName());
        }
    }

    public static void main(String[] args) {

        for (Day.Weekday day : weekdays) {
            System.out.println("" +
                    "The day is " + day.getEnglishName() + "/" + day.getRomanianName() + ", " +
                    (day.ordinal()+1) + ". day of week, " +
                    (day.isWeekDay() ? "weekday" : "holiday"));
        }

        for(int i=0; i<10; i++) {
            int a,b;
            a = (int) (Math.random()*100%7);
            b = (int) (Math.random()*100%7);

            printLogicalRelation(a,b);
        }
    }
}
