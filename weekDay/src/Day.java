public class Day {

    private Weekday weekday;
    private int date;

    enum Weekday {
        MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY;

        public boolean isWeekDay () {
            return this.ordinal() <= 4;
        }

        public boolean isHoliday () {
            int ord = this.ordinal();
            return ord >= 5 && ord <= 6;
        }

        public String getEnglishName () {
            switch (this) {
                case MONDAY:
                    return "Monday";
                case TUESDAY:
                    return "Tuesday";
                case WEDNESDAY:
                    return "Wednesday";
                case THURSDAY:
                    return "Thursday";
                case FRIDAY:
                    return "Friday";
                case SATURDAY:
                    return "Saturday";
                case SUNDAY:
                    return "Sunday";
                default:
                    return null;
            }
        }

        public String getRomanianName () {
            switch (this) {
                case MONDAY:
                    return "Luni";
                case TUESDAY:
                    return "Marti";
                case WEDNESDAY:
                    return "Mercuri";
                case THURSDAY:
                    return "Joi";
                case FRIDAY:
                    return "Vineri";
                case SATURDAY:
                    return "Sambata";
                case SUNDAY:
                    return "Duminica";
                default:
                    return null;
            }
        }

    }
}
