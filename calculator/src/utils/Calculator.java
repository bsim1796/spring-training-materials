package utils;

public class Calculator {

    public static float calculate (String formula) throws FormulaError {
        String[] list = formula.split(" ");
        if (list.length != 3) {
            throw new FormulaError("The formula must consist 3 elements separated with space (eg. 1 + 1)");
        }
        float a,b;
        try {
            a = Float.parseFloat(list[0]);
            b = Float.parseFloat(list[2]);
        } catch (NumberFormatException e) {
            throw new FormulaError("The first and third element is not a float (can not parse). Eg. for a float: 23.5");
        }

        if (list[1].equals("+")) {
            return a+b;
        }
        else if (list[1].equals("-")){
            return a-b;
        }
        else {
            throw new FormulaError("The second argument must be '+' or '-'");
        }
    }
}
