import utils.Calculator;
import utils.FormulaError;

public class Main {
    public static void main(String[] args) {

        try {
            System.out.println("" + Calculator.calculate("15.55 + 48.62"));
            System.out.println("" + Calculator.calculate("15.55 - 48.62"));
            System.out.println("" + Calculator.calculate("foo x 48.62"));
        } catch (FormulaError formulaError) {
            formulaError.printStackTrace();
        }
    }
}
