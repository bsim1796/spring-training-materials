import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Coach bestCoach = context.getBean("myFootballCoach", Coach.class);
        System.out.println(bestCoach.getDailyWorkout());
        System.out.println(bestCoach.getDailyFortune());

        Coach betterCoach = context.getBean("myFootballCoach", Coach.class);

        System.out.println("\nPointing to the same object: " + (bestCoach==betterCoach));

        System.out.println("\nMemory location for theCoach: " + bestCoach);

        System.out.println("\nMemory location for alphaCoach: " + betterCoach + "\n");

        context.close();
    }
}
