import exceptions.BasicClass;
import exceptions.BasicLogged;
import exceptions.leveldebug.DebugLogged;
import exceptions.levelerror.ErrorLogged;
import exceptions.levelinfo.InfoLogged;

public class Main {
    public static void main(String[] args) {

        BasicLogged debugLogged = new DebugLogged();
        debugLogged.setWeight(55);
        debugLogged.setMessages("It's a lightweight");
        debugLogged.setBasicClass(DebugLogged.class);
        int debugLoggedWeight = debugLogged.getWeight();
        String debugLoggedMessage = debugLogged.getMessages();
        Object debugLoggedBasicClass = debugLogged.getBasicClass();
        boolean debugLoggedOverweight = debugLogged.isOverweight();
        try {
            debugLogged.evaluateBasicClass();
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }

        BasicLogged errorLogged = new ErrorLogged();
        errorLogged.setWeight(180);
        errorLogged.setMessages("It's a giant");
        errorLogged.setBasicClass(BasicClass.class);
        int errorLoggedWeight = errorLogged.getWeight();
        String errorLoggedMessage = errorLogged.getMessages();
        Object errorLoggedBasicClass = errorLogged.getBasicClass();
        boolean errorLoggedOverweight = errorLogged.isOverweight();
        try {
            errorLogged.evaluateBasicClass();
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }

        BasicLogged infoLogged = new InfoLogged();
        infoLogged.setWeight(91);
        infoLogged.setMessages("It's so-so");
        infoLogged.setBasicClass(DebugLogged.class);
        int infoLoggedWeight = infoLogged.getWeight();
        String infoLoggedMessage = infoLogged.getMessages();
        Object infoLoggedBasicClass = infoLogged.getBasicClass();
        boolean infoLoggedOverweight = infoLogged.isOverweight();
        try {
            infoLogged.evaluateBasicClass();
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }
    }
}
