package exceptions;

public interface BasicLogged {
    int getWeight();
    void setWeight(int weight);

    String getMessages();
    void setMessages(String messages);

    Object getBasicClass();
    void setBasicClass(Object basicClass);

    boolean isOverweight();
    void evaluateBasicClass() throws RuntimeException;
}
