import controller.atm.DefaultATM;
import view.Application;
import view.ConsoleApplication;

public class Main {
    private static final DefaultATM DEFAULT_ATM = new DefaultATM();

    private static final Application application = new ConsoleApplication();
    public static void main(String[] args) {
        application.start(DEFAULT_ATM.getATMById(1));
    }
}
