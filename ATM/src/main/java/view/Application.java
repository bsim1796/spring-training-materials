package view;

import model.ATM;

public interface Application {
    void start(ATM atm);
}
