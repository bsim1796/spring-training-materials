package view;

import controller.AccountController;
import controller.UserController;
import controller.account.DefaultAccount;
import controller.user.DefaultUser;
import dao.DaoException;
import model.ATM;
import model.Account;
import model.Transaction;
import model.User;

import java.util.HashMap;
import java.util.Scanner;

public class ConsoleApplication implements Application{

    private static final UserController USER_CONTROLLER = new DefaultUser();
    private static final AccountController ACCOUNT_CONTROLLER = new DefaultAccount();
    private static final Scanner SCANNER = new Scanner(System.in);
    private static final int ATTEMPTS = 5;

    private static User user;
    private static HashMap<Long,Account> accounts = new HashMap<>();


    private void login() {
        int attempts = ATTEMPTS;
        for(;attempts>0;attempts--) {
            if(attempts!= 5) {
                System.out.println("Attempts left: " + attempts);
            }
            System.out.println("Welcome at ATM machine! Please enter your credentials...\nUnique indentifier:");
            String id = SCANNER.nextLine();
            if (id.length() != 9) {
                System.out.println("The identifier must be exactly 9 digits");
                continue;
            }
            long identifier;
            try{
                identifier = Long.parseLong(id);
                //I check this way if any DaoException is thrown
                User tempUser = USER_CONTROLLER.getUserByIdentifier(identifier);
            } catch (NumberFormatException e){
                System.out.println("The identifier must be exactly 9 digits");
                continue;
            } catch (DaoException e) {
                System.out.println(e.getMessage());
                continue;
            }
            System.out.println("Enter pin code:");
            String pin = SCANNER.nextLine();
            if (pin.length() != 4) {
                System.out.println("The pin must be exactly 4 digits");
                continue;
            }
            int pinCode;
            try{
                pinCode = Integer.parseInt(pin);
            } catch (NumberFormatException e){
                System.out.println("The pin must be exactly 4 digits");
                continue;
            }
            User tempUser = USER_CONTROLLER.getUserByIdentifier(identifier);
            if (USER_CONTROLLER.authenticate(tempUser, pinCode)) {
                user = tempUser;
                for (Account account:USER_CONTROLLER.getAccountsByUserId(user.getId())) {
                    accounts.put(account.getId(),account);
                }
                break;
            }
            else{
                System.out.println("PIN code is incorrect");
            }
        }
        if(attempts == 0){
            System.out.println("Too many unsuccessful attempts. Come back later!");
            System.exit(0);
        }
    }

    private static void mainMenu() {
        System.out.println("Enter a command's code:\n" +
                "Show balance - 1\n" +
                "Show transactions - 2\n" +
                "Withdraw - 3\n" +
                "Deposit - 4\n" +
                "Transfer - 5\n" +
                "Quit - 6");
    }

    private static boolean anotherOperation() {
        while (true) {
            System.out.println("Do you want to perform an other operation? (yes/no)");
            String command = SCANNER.nextLine();
            switch (command) {
                case "yes":
                case "y":
                    return false;
                case "no":
                case "n":
                    return true;
                default:
                    System.out.println("Invalid command...");
            }
        }
    }

    private static long selectAccount() {
        for (Long id:accounts.keySet()) {
            System.out.println(accounts.get(id).getType() + " " + accounts.get(id).getIBAN() + " "
                    + accounts.get(id).getCurrency() + " --> " + accounts.get(id).getId());
        }
        long accountId;
        do{
            accountId = Long.parseLong(SCANNER.nextLine());
            if(accounts.containsKey(accountId)){
                break;
            } else {
                System.out.println("Invalid account id. Enter a correct account id:");
            }
        }while (true);
        return accountId;
    }

    private static void showBalance() {
        System.out.println("Select an account:");
        long accountId = selectAccount();
        System.out.println("The balance is: " + ACCOUNT_CONTROLLER.getAccountById(accountId).getBalance());
        if(anotherOperation()) {
            System.exit(0);
        }
    }

    private static void showTransactions() {
        System.out.println("Your transactions: ");
        for (Transaction transaction:USER_CONTROLLER.getTransactionsByUserId(user.getId())) {
            System.out.println(transaction);
        }
        if(anotherOperation()) {
            System.exit(0);
        }
    }


    private static void withdraw(ATM atm) {
        System.out.println("Select an account:");
        long accountId = selectAccount();
        float amount;
        while(true){
            System.out.println("Enter the amount: ");
            amount = Float.parseFloat(SCANNER.nextLine());
            if(accounts.get(accountId).getBalance() < amount) {
                System.out.println("You dont have this much. Enter a valid amount or enter 0 to return to the main menu.");
            }
            else if (amount == 0){
                return;
            }
            else {
                break;
            }
        }
        //if the atm's bank is not the same then the user's account bank, penalty is applied
        if(accounts.get(accountId).getBank().isTheSameBank(atm.getBank())) {
            accounts.get(accountId).setBalance(accounts.get(accountId).getBalance() - amount);
        }
        else {
            accounts.get(accountId).setBalance(accounts.get(accountId).getBalance() - amount - ((amount*atm.getBank().getTransferFeePercent())/100));
        }
        System.out.println("Your new balance is: " + accounts.get(accountId).getBalance());
        if(anotherOperation()) {
            System.exit(0);
        }
    }

    private static void deposit(ATM atm) {
        System.out.println("Select an account:");
        long accountId = selectAccount();
        System.out.println("Enter the amount: ");
        float amount = Float.parseFloat(SCANNER.nextLine());
        //if the atm's bank is not the same then the user's account bank, penalty is applied
        if(accounts.get(accountId).getBank().isTheSameBank(atm.getBank())) {
            accounts.get(accountId).setBalance(accounts.get(accountId).getBalance() - amount);
        }
        else {
            accounts.get(accountId).setBalance(accounts.get(accountId).getBalance() + amount - ((amount*atm.getBank().getTransferFeePercent())/100));
        }
        System.out.println("Your new balance is: " + accounts.get(accountId).getBalance());
        if(anotherOperation()) {
            System.exit(0);
        }
    }

    private static void transfer(ATM atm) {
        System.out.println("Select an account:");
        long accountId = selectAccount();
        float amount;
        while(true){
            System.out.println("Enter the amount: ");
            amount = Float.parseFloat(SCANNER.nextLine());
            if(accounts.get(accountId).getBalance() < amount) {
                System.out.println("You dont have this much to transfer. Enter a valid amount or enter 0 to return to the main menu.");
            }
            else if (amount == 0){
                return;
            }
            else {
                break;
            }
        }
        String iban;
        Account destination;
        final String EXIT = "exit";
        while (true) {
            System.out.println("Enter the destinations IBAN: ");
            iban = SCANNER.nextLine();
            if(EXIT.equals(iban)) {
                return;
            }
            try{
                destination = ACCOUNT_CONTROLLER.getAccountByIBAN(iban);
            } catch (DaoException e) {
                System.out.println("Account with this IBAN not found. Enter a valid iban or enter 'exit' to quit to the main menu.");
                continue;
            }
            break;
        }
        //if the atm's bank is not the same then the user's account bank, penalty is applied
        if(accounts.get(accountId).getBank().isTheSameBank(atm.getBank())) {
            accounts.get(accountId).setBalance(accounts.get(accountId).getBalance() - amount);
        }
        else {
            accounts.get(accountId).setBalance(accounts.get(accountId).getBalance() - amount - ((amount*atm.getBank().getTransferFeePercent())/100));
        }
        System.out.println("Transfer successful. Your new balance is: " + accounts.get(accountId).getBalance());
        if(anotherOperation()) {
            System.exit(0);
        }
    }

    @Override
    public void start(ATM atm) {
        login();
        System.out.println("Welcome " + user.getName() + "!");
        while(true) {
            mainMenu();
            int command;
            try {
                command = Integer.parseInt(SCANNER.nextLine());
                switch (command) {
                    case 1:
                        showBalance();
                        break;
                    case 2:
                        showTransactions();
                        break;
                    case 3:
                        withdraw(atm);
                        break;
                    case 4 :
                        deposit(atm);
                        break;
                    case 5:
                        transfer(atm);
                        break;
                    case 6:
                        System.out.println("Have a nice day!");
                        return;
                    default:
                        System.out.println("Invalid command...");
                }
            } catch (NumberFormatException e) {
                System.out.println("Invalid command...");
            }
        }
    }
}
