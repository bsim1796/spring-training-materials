package model;

import utils.Currency;

public class Account extends BaseEntity {
    private String type;
    private String IBAN;
    private double balance;
    private Currency currency;
    private User user;
    private Bank bank;

    public Account() {
    }

    public Account(int id, String type, String iban, double balance, Currency currency, User user, Bank bank) {
        super(id);
        this.type = type;
        IBAN = iban;
        this.balance = balance;
        this.currency = currency;
        this.user = user;
        this.bank = bank;
    }

    public double getBalance() {
        return balance;
    }

    public Currency getCurrency() {
        return currency;
    }

    public String getIBAN() {
        return IBAN;
    }

    public void setIBAN(String IBAN) {
        this.IBAN = IBAN;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    @Override
    public String toString() {
        return "Account{" +
                ", ID='" + this.getId() + '\'' +
                ", type='" + type + '\'' +
                ", balance=" + balance +
                ", currency=" + currency +
                '}';
    }
}
