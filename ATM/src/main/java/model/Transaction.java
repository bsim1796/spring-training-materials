package model;

import java.util.Date;

public class Transaction extends BaseEntity {
    private Account senderAccount;
    private Account destinationAccount;
    private double amount;
    private Date date;

    public Transaction(int id, Account senderAccount, Account destinationAccount, double amount, Date date) {
        super(id);
        this.senderAccount = senderAccount;
        this.destinationAccount = destinationAccount;
        this.amount = amount;
        this.date = date;
    }

    public Transaction() {
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Account getSenderAccount() {
        return senderAccount;
    }

    public void setSenderAccount(Account senderAccount) {
        this.senderAccount = senderAccount;
    }

    public Account getDestinationAccount() {
        return destinationAccount;
    }

    public void setDestinationAccount(Account destinationAccount) {
        this.destinationAccount = destinationAccount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Transaction{");
        sb.append("id=").append(this.getId());
        sb.append(", senderAccountId=").append(senderAccount);
        sb.append(", destinationAccountId=").append(destinationAccount);
        sb.append(", amount=").append(amount);
        sb.append(", date=").append(date);
        sb.append('}');
        return sb.toString();
    }
}
