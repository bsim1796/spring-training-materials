package model;

public class ATM extends BaseEntity {
    private String address;
    private Bank bank;

    public ATM() {
    }

    public ATM(int id, String address, Bank bank) {
        super(id);
        this.address = address;
        this.bank = bank;
    }

    public String getAddress() {
        return address;
    }

    public Bank getBank() {
        return bank;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ATM{");
        sb.append("address='").append(address).append('\'');
        sb.append(", bank=").append(bank);
        sb.append('}');
        return sb.toString();
    }
}
