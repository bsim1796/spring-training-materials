package model;

public abstract class BaseEntity {
    private long ID;

    public void setID(long ID) {
        this.ID = ID;
    }

    public long getId() {
        return ID;
    }

    public BaseEntity() {
    }

    public BaseEntity(long id) {
        ID = id;
    }
}
