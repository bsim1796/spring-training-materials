package model;

public class User extends BaseEntity {
    private long identifier;
    private int pinCode;
    private String name;
    private int age;
    private String email;

    public User() {
    }

    public User(int id, String name, int age, String email, long identifier, int pinCode, Bank bank) {
        super(id);
        this.name = name;
        this.age = age;
        this.email = email;
        this.identifier = identifier;
        this.pinCode = pinCode;
    }

    public void setIdentifier(long identifier) {
        this.identifier = identifier;
    }

    public void setPinCode(int pinCode) {
        this.pinCode = pinCode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getIdentifier() {
        return identifier;
    }

    public int getPinCode() {
        return pinCode;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "User{" +
                "ID='" + super.getId() + '\'' +
                ", identifier='" + identifier + '\'' +
                ", pinCode=" + pinCode +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", email='" + email + '\'' +
                '}';
    }
}
