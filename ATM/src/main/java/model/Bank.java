package model;

public class Bank extends BaseEntity {
    private String name;
    private float transferFeePercent;

    public Bank() {
    }

    public Bank(int id, String name, float transferFee) {
        super(id);
        this.name = name;
        this.transferFeePercent = transferFee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getTransferFeePercent() {
        return transferFeePercent;
    }

    public void setTransferFeePercent(float transferFeePercent) {
        this.transferFeePercent = transferFeePercent;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Bank{");
        sb.append("name='").append(name).append('\'');
        sb.append(", transferFeePercent=").append(transferFeePercent);
        sb.append('}');
        return sb.toString();
    }

    public boolean isTheSameBank(Bank bank) {
        return this.getName().equals(bank.getName());
    }
}
