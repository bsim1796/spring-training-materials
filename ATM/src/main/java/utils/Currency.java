package utils;

public enum Currency {
    EURO, USD, RON, HUF, PESO, DINAR;
}
