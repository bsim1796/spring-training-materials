package controller.user;

import controller.UserController;
import dao.AbstractDaoFactory;
import dao.UserDao;
import model.Account;
import model.Transaction;
import model.User;

import java.util.Collection;

public class DefaultUser implements UserController {

    private static final UserDao USER_DAO = AbstractDaoFactory.getInstance("MEM").getUserDao();

    @Override
    public Collection<User> getAllUsers() {
        return USER_DAO.get();
    }

    @Override
    public User getUserById(Long id){
        return USER_DAO.getById(id);
    }

    @Override
    public User getUserByIdentifier(long identifier) {
        return USER_DAO.getByIdentifier(identifier);
    }


    @Override
    public boolean authenticate(User user, int enteredPinCode) {
        return user.getPinCode() == enteredPinCode;
    }

    @Override
    public Collection<Account> getAccountsByUserId(long id) {
        return USER_DAO.getAccountsByUserId(id);
    }

    @Override
    public Collection<Transaction> getTransactionsByUserId(long id) {
        return USER_DAO.getTransactionsByUserId(id);
    }
}
