package controller.account;

import controller.AccountController;
import dao.AbstractDaoFactory;
import dao.AccountDao;
import model.Account;

import java.util.Collection;

public class DefaultAccount implements AccountController {

    private static final AccountDao ACCOUNT_DAO = AbstractDaoFactory.getInstance("MEM").getAccountDao();

    @Override
    public Collection<Account> getAllAccount() {
        return ACCOUNT_DAO.get();
    }

    @Override
    public Account getAccountById(long id) {
        return ACCOUNT_DAO.getById(id);
    }

    @Override
    public Account getAccountByIBAN(String IBAN) {
        return ACCOUNT_DAO.getByIBAN(IBAN);
    }
}
