package controller;

import model.ATM;

import java.util.Collection;

public interface ATMController {
    Collection<ATM> getAllATM();

    ATM getATMById(long id);
}
