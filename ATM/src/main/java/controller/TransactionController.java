package controller;

import model.Transaction;

import java.util.Collection;

public interface TransactionController {
    Collection<Transaction> getAllTransaction();

    Transaction getTransactionById(long id);
}
