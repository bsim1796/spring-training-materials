package controller;

import model.Account;

import java.util.Collection;

public interface AccountController {
    Collection<Account> getAllAccount();

    Account getAccountById(long id);

    Account getAccountByIBAN(String IBAN);
}
