package controller.transaction;

import controller.TransactionController;
import dao.AbstractDaoFactory;
import dao.TransactionDao;
import model.Transaction;

import java.util.Collection;

public class DefaultTransaction implements TransactionController {

    private static final TransactionDao TRANSACTION_DAO = AbstractDaoFactory.getInstance("MEM").getTransactionDao();

    @Override
    public Collection<Transaction> getAllTransaction() {
        return TRANSACTION_DAO.get();
    }

    @Override
    public Transaction getTransactionById(long id) {
        return TRANSACTION_DAO.getById(id);
    }
}
