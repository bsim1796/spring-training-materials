package controller;

import model.Bank;

import java.util.Collection;

public interface BankController {

    Collection<Bank> getAllBank();

    Bank getBankById(long id);
}
