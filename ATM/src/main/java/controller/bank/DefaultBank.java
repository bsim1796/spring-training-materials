package controller.bank;

import controller.BankController;
import dao.AbstractDaoFactory;
import dao.BankDao;
import model.Bank;

import java.util.Collection;

public class DefaultBank implements BankController {

    private static final BankDao BANK_DAO = AbstractDaoFactory.getInstance("MEM").getBankDao();

    @Override
    public Collection<Bank> getAllBank() {
        return BANK_DAO.get();
    }

    @Override
    public Bank getBankById(long id) {
        return BANK_DAO.getById(id);
    }
}
