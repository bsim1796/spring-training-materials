package controller.atm;

import controller.ATMController;
import dao.ATMDao;
import dao.AbstractDaoFactory;
import model.ATM;

import java.util.Collection;

public class DefaultATM implements ATMController {

    private static final ATMDao ATM_DAO = AbstractDaoFactory.getInstance("MEM").getATMDao();

    @Override
    public Collection<ATM> getAllATM() {
        return ATM_DAO.get();
    }

    @Override
    public ATM getATMById(long id) {
        return ATM_DAO.getById(id);
    }
}
