package controller;

import model.Account;
import model.Transaction;
import model.User;

import java.util.Collection;

public interface UserController {
    Collection<User> getAllUsers();

    User getUserById(Long id);

    User getUserByIdentifier(long identifier);

    boolean authenticate(User user, int enteredPinCode);

    Collection<Account> getAccountsByUserId(long id);

    Collection<Transaction> getTransactionsByUserId (long id);
}
