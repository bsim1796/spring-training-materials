package dao;

import model.Transaction;

public interface TransactionDao extends Dao<Transaction> {
}
