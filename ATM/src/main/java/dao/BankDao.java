package dao;

import model.Bank;

public interface BankDao extends Dao<Bank> {
}
