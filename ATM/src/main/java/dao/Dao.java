package dao;

import model.BaseEntity;

import java.io.Serializable;
import java.util.Collection;

public interface Dao <T extends BaseEntity> extends Serializable {
    Collection<T> get();

    T getById(long id);

}
