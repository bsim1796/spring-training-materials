package dao;

import model.Account;

public interface AccountDao extends Dao<Account>{
    Account getByIBAN(String iban);
}
