package dao;

import dao.memory.MemoryDaoFactory;

public abstract class AbstractDaoFactory {

    private  static  AbstractDaoFactory instance;

    public abstract UserDao getUserDao();

    public abstract AccountDao getAccountDao();

    public abstract BankDao getBankDao();

    public abstract ATMDao getATMDao();

    public abstract TransactionDao getTransactionDao();

    public static synchronized AbstractDaoFactory getInstance(String daoType) {
        //we can read this daoType from a configuration file later
        if (instance == null) {
            if (daoType.equals("MEM")) {
                instance = new MemoryDaoFactory();
            } else {
                throw new DaoException("Unable to read a supported dao type: " + daoType);
            }
        }
        return instance;
    }

}
