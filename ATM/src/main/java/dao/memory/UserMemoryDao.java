package dao.memory;

import dao.*;
import model.Account;
import model.Transaction;
import model.User;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class UserMemoryDao implements UserDao {

    private static void validateUser (User user, String[] data) {
        try {
            user.setID(Long.parseLong(data[0]));
            user.setIdentifier(Long.parseLong(data[1]));
            user.setPinCode(Integer.parseInt(data[2]));
            user.setName(data[3]);
            user.setAge(Integer.parseInt(data[4]));
            user.setEmail(data[5]);
        } catch (NumberFormatException e) {
            throw new DaoException("User in a wrong format in the file");
        }
    }

    @Override
    public Collection<User> get() {
        Collection<User> users = new ArrayList<>();
        try(BufferedReader reader = new BufferedReader(
                new FileReader("src/main/resources/users.txt"))) {
            String line = reader.readLine();
            while (line != null) {
                String[] data = line.split(",");
                User user = new User();
                validateUser(user,data);
                users.add(user);
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            System.out.println("The file not found");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IO exception occurred");
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public User getById(long id) {
        List<User> users = this.get().stream().filter(x -> x.getId() == id ).collect(Collectors.toList());
        if (users.size() != 1) {
            throw new DaoException("User By Id not found (or found too many)");
        }
        return users.get(0);
    }

    @Override
    public User getByIdentifier(Long identifier) {
        List<User> users = this.get().stream().filter(x -> x.getIdentifier() == identifier ).collect(Collectors.toList());
        if (users.size() != 1) {
            throw new DaoException("User By identifier not found (or found too many)");
        }
        return users.get(0);
    }

    @Override
    public Collection<Account> getAccountsByUserId(long id) {
        AccountDao ACCOUNT_DAO = AbstractDaoFactory.getInstance("MEM").getAccountDao();
        return ACCOUNT_DAO.get().stream().filter(x -> x.getUser().getId() == id ).collect(Collectors.toList());
    }

    @Override
    public Collection<Transaction> getTransactionsByUserId(long id) {
        TransactionDao TRANSACTION_DAO = AbstractDaoFactory.getInstance("MEM").getTransactionDao();
        return TRANSACTION_DAO.get().stream().filter(x -> x.getSenderAccount().getUser().getId() == id).collect(Collectors.toList());
    }


}
