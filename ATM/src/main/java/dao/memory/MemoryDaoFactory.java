package dao.memory;

import dao.*;

public class MemoryDaoFactory extends AbstractDaoFactory {
    public static final UserDao USER_MEMORY_DAO = new UserMemoryDao();
    public static final AccountDao ACCOUNT_MEMORY_DAO = new AccountMemoryDao();
    public static final TransactionDao TRANSACTION_MEMORY_DAO = new TransactionMemoryDao();
    public static final ATMDao ATM_MEMORY_DAO = new ATMMemoryDao();
    public static final BankDao BANK_MEMORY_DAO = new BankMemoryDao();

    @Override
    public UserDao getUserDao() {
        return USER_MEMORY_DAO;
    }

    @Override
    public AccountDao getAccountDao() {
        return ACCOUNT_MEMORY_DAO;
    }

    @Override
    public BankDao getBankDao() {
        return BANK_MEMORY_DAO;
    }

    @Override
    public ATMDao getATMDao() {
        return ATM_MEMORY_DAO;
    }

    @Override
    public TransactionDao getTransactionDao() {
        return TRANSACTION_MEMORY_DAO;
    }


}
