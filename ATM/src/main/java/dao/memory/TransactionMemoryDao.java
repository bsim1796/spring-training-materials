package dao.memory;

import dao.AbstractDaoFactory;
import dao.AccountDao;
import dao.DaoException;
import dao.TransactionDao;
import model.Transaction;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class TransactionMemoryDao implements TransactionDao {

    private static final TransactionDao TRANSACTION_DAO = AbstractDaoFactory.getInstance("MEM").getTransactionDao();

    private static void validateTransaction (Transaction transaction, String[] data) {
        final AccountDao ACCOUNT_DAO = AbstractDaoFactory.getInstance("MEM").getAccountDao();
        try {
            transaction.setID(Long.parseLong(data[0]));
            transaction.setSenderAccount(ACCOUNT_DAO.getById(Long.parseLong(data[1])));
            transaction.setDestinationAccount(ACCOUNT_DAO.getById(Long.parseLong(data[2])));
            transaction.setAmount(Double.parseDouble(data[3]));
            transaction.setDate(new SimpleDateFormat("dd/MM/yyyy").parse(data[4]));

        } catch (NumberFormatException | ParseException e) {
            throw new DaoException("Account in a wrong format in the file");
        }
    }


    @Override
    public Collection<Transaction> get() {
        Collection<Transaction> transactions = new ArrayList<>();
        try(BufferedReader reader = new BufferedReader(
                new FileReader("src/main/resources/transactions.txt"))) {
            String line = reader.readLine();
            while (line != null) {
                String[] data = line.split(",");
                Transaction transaction = new Transaction();
                validateTransaction(transaction,data);
                transactions.add(transaction);
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            System.out.println("The file not found");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IO exception occurred");
            e.printStackTrace();
        }
        return transactions;
    }

    @Override
    public Transaction getById(long id) {
        System.out.println(id);
        List<Transaction> transactions = this.get().stream().filter(x -> x.getId() == id ).collect(Collectors.toList());
        if (transactions.size() != 1) {
            throw new DaoException("Transaction By Id not found (or found too many)");
        }
        return transactions.get(0);
    }
}
