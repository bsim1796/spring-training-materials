package dao.memory;

import dao.ATMDao;
import dao.AbstractDaoFactory;
import dao.BankDao;
import dao.DaoException;
import model.ATM;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class ATMMemoryDao implements ATMDao {

    private static void validateATM (ATM atm, String[] data) {
        BankDao BANK_DAO = AbstractDaoFactory.getInstance("MEM").getBankDao();
        try {
            atm.setID(Long.parseLong(data[0]));
            atm.setAddress(data[1]);
            atm.setBank(BANK_DAO.getById(Long.parseLong(data[2])));
        } catch (NumberFormatException e) {
            throw new DaoException("ATM in a wrong format in the file");
        }
    }

    @Override
    public Collection<ATM> get() {
        Collection<ATM> atms = new ArrayList<>();
        try(BufferedReader reader = new BufferedReader(
                new FileReader("src/main/resources/atms.txt"))) {
            String line = reader.readLine();
            while (line != null) {
                String[] data = line.split(",");
                ATM atm = new ATM();
                validateATM(atm,data);
                atms.add(atm);
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            System.out.println("The file not found");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IO exception occurred");
            e.printStackTrace();
        }
        return atms;
    }

    @Override
    public ATM getById(long id) {
        List<ATM> atms = this.get().stream().filter(x -> x.getId() == id ).collect(Collectors.toList());
        if (atms.size() != 1) {
            throw new DaoException("ATM By Id not found (or found too many)");
        }
        return atms.get(0);
    }
}
