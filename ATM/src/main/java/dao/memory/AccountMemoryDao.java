package dao.memory;

import dao.*;
import model.Account;
import model.Bank;
import utils.Currency;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class AccountMemoryDao implements AccountDao {
    private static final UserDao USER_DAO = AbstractDaoFactory.getInstance("MEM").getUserDao();

    private static void validateAccount (Account account, String[] data) {
        BankDao BANK_DAO = AbstractDaoFactory.getInstance("MEM").getBankDao();
        try {
            account.setID(Long.parseLong(data[0]));
            account.setType(data[1]);
            account.setIBAN((data[2]));
            account.setBalance(Double.parseDouble(data[3]));
            account.setCurrency(Currency.valueOf(data[4]));
            account.setUser(USER_DAO.getById(Integer.parseInt(data[5])));
            Bank bank = BANK_DAO.getById(Long.parseLong(data[6]));
            account.setBank(bank);
        } catch (NumberFormatException e) {
            throw new DaoException("Account in a wrong format in the file");
        }
    }

    @Override
    public Collection<Account> get() {
        Collection<Account> accounts = new ArrayList<>();
        try(BufferedReader reader = new BufferedReader(
                new FileReader("src/main/resources/accounts.txt"))) {
            String line = reader.readLine();
            while (line != null) {
                String[] data = line.split(",");
                Account account = new Account();
                validateAccount(account,data);
                accounts.add(account);
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            System.out.println("The file not found");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IO exception occurred");
            e.printStackTrace();
        }
        return accounts;
    }

    @Override
    public Account getById(long id) {
        List<Account> accounts = this.get().stream().filter(x -> x.getId() == id ).collect(Collectors.toList());
        if (accounts.size() != 1) {
            throw new DaoException("Account By id not found (or found too many) " + id);
        }
        return accounts.get(0);
    }

    @Override
    public Account getByIBAN(String iban) {
        List<Account> accounts = this.get().stream().filter(x -> Objects.equals(x.getIBAN(), iban)).collect(Collectors.toList());
        if (accounts.size() != 1) {
            throw new DaoException("Account By IBAN not found (or found too many)");
        }
        return accounts.get(0);
    }
}
