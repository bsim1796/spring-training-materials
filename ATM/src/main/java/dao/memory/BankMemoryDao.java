package dao.memory;

import dao.BankDao;
import dao.DaoException;
import model.Bank;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class BankMemoryDao implements BankDao {

    private static void validateBank (Bank bank, String[] data) {
        try {
            bank.setID(Long.parseLong(data[0]));
            bank.setName(data[1]);
            bank.setTransferFeePercent(Float.parseFloat(data[2]));
        } catch (NumberFormatException e) {
            throw new DaoException("Bank in a wrong format in the file");
        }
    }

    @Override
    public Collection<Bank> get() {
        Collection<Bank> banks = new ArrayList<>();
        try(BufferedReader reader = new BufferedReader(
                new FileReader("src/main/resources/banks.txt"))) {
            String line = reader.readLine();
            while (line != null) {
                String[] data = line.split(",");
                Bank bank = new Bank();
                validateBank(bank,data);
                banks.add(bank);
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            System.out.println("The file not found");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IO exception occurred");
            e.printStackTrace();
        }
        return banks;
    }

    @Override
    public Bank getById(long id) {
        List<Bank> banks = this.get().stream().filter(x -> x.getId() == id ).collect(Collectors.toList());
        if (banks.size() != 1) {
            throw new DaoException("ATM By Id not found (or found too many)");
        }
        return banks.get(0);
    }
}
