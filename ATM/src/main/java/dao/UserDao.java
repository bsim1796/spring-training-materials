package dao;

import model.Account;
import model.Transaction;
import model.User;

import java.util.Collection;

public interface UserDao extends Dao<User> {

    User getByIdentifier(Long identifier);

    Collection<Account> getAccountsByUserId(long id);

    Collection<Transaction> getTransactionsByUserId(long id);
}
