package edu.spring.training;

import org.springframework.stereotype.Component;

@Component
public class ChessCoach implements Coach{

    private FortuneService fortuneService;

    public ChessCoach(FortuneService fortuneService) {
        this.fortuneService = fortuneService;
    }

    @Override
    public String getDailyWorkout() {
        return "Win 12 games";
    }

    @Override
    public String getDailyFortune() {
        return fortuneService.getFortune();
    }
}
