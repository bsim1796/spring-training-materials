package edu.spring.training;

public interface Coach {

    public String getDailyWorkout();

    public String getDailyFortune();
}
