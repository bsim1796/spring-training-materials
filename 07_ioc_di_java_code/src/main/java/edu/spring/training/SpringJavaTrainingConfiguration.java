package edu.spring.training;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringJavaTrainingConfiguration {
    @Bean
    public FortuneService anOtherFortuneService() {
        return new AnOtherFortuneService();
    }

    @Bean
    public Coach chessCoach() {
        ChessCoach myChessCoach = new ChessCoach(anOtherFortuneService());
        return myChessCoach;
    }
}
