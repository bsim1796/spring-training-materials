package edu.spring.training;

public class AnOtherFortuneService implements FortuneService{
    @Override
    public String getFortune() {
        return "May the force be with you!";
    }
}
