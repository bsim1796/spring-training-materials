package hashmap;

import java.util.*;

public class Countries {
    private HashMap<String,String> hmCountry;

    public Countries() {
        hmCountry = new HashMap<>();
    }

    public void addCountries() {
        hmCountry.put("ES", "Spain");
        hmCountry.put("DE", "Germany");
        hmCountry.put("FR", "France");
        hmCountry.put("CN", "China");
        hmCountry.put("US", "United States");
        hmCountry.put("RU", "Russia");
        hmCountry.put("GB", "United Kingdom");
        hmCountry.put("IN", "India");
        hmCountry.put("BR", "Brazil");
        hmCountry.put("SA", "South Africa");
    }

    public HashMap<String, String> getHmCountry() {
        return hmCountry;
    }

    public void printMapElements () {
        for (HashMap.Entry<String, String> country : hmCountry.entrySet()) {
            System.out.println("Key: " + country.getKey() + ", Value: " + country.getValue());
        }
    }
}
