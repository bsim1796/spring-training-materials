import hashmap.Countries;
import list.Colors;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        ///First task - Hashmap
        Countries countries = new Countries();
        System.out.println("Size before: " + countries.getHmCountry().size());
        countries.addCountries();
        System.out.println("Size after: " + countries.getHmCountry().size());
        countries.printMapElements();
        System.out.println(countries.getHmCountry().get("CN"));
        countries.getHmCountry().remove("GB");
        countries.getHmCountry().putIfAbsent("RO", "Romania");
        System.out.println(
                (countries.getHmCountry().containsKey("SA"))
                        ? "It contains key SA"
                        : "Does not contains key SA"
        );
        System.out.println(
                (countries.getHmCountry().containsValue("Spain"))
                        ? "It contains value Spain"
                        : "Does not contains value Spain"
        );
        countries.getHmCountry().remove("IN", "India");
        //sorting the map using java 8 streams
        countries.getHmCountry().entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .forEach(System.out::println);
        /// 2. list
        Colors colors = new Colors();
        colors.add4Color();
        colors.insertFirst();
        colors.insertLast();
        colors.sort();
        colors.iterateThroughArray();
        ArrayList<String> colors2 = colors.cloneList();
        System.out.println(colors2);
        colors.joinArrayLists(colors2);
        colors.iterateThroughArray();
        //3.
        List<Integer> randomList = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            randomList.add((int) ((Math.random() * 10000) % 50));
        }
        Set<Integer> uniques = new HashSet<>(randomList);
        System.out.println("The unique elements are: " + uniques);
        Set<Integer> duplicates = new HashSet<>();
        Set<Integer> temp = new HashSet<>();
        for (Integer element : randomList) {
            if (!temp.add(element)) {
                duplicates.add(element);
            }
        }
        System.out.println("The duplicated elements are" + duplicates);

        ///4.
        List<String> arrayList = new ArrayList<>();
        arrayList.add("a");
        arrayList.add("a");
        arrayList.add("a");
        arrayList.add("a");
        arrayList.add("b");
        arrayList.add("b");
        arrayList.add("b");
        arrayList.add("c");
        arrayList.add("c");
        arrayList.add("c");
        arrayList.add("d");
        arrayList.add("d");
        arrayList.add("d");
        arrayList.add("d");
        Set<String> set = new HashSet<>(arrayList);
        arrayList.clear();
        arrayList.addAll(set);
    }
}
