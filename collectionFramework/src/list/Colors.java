package list;

import java.util.ArrayList;

import java.util.Collections;

public class Colors {
    private ArrayList<String> colors = new ArrayList<>();

    public void add4Color() {
        colors.add("Blue");
        colors.add("Red");
        colors.add("Yellow");
        colors.add("Black");
    }

    public ArrayList<String> getColors() {
        return colors;
    }

    public void iterateThroughArray() {
        colors.forEach((n) -> System.out.println(n));
    }

    public void insertLast() {
        colors.add(colors.size(),"White");
    }

    public void insertFirst() {
        colors.add(0,"Pink");
    }

    public ArrayList<String> getFirstAndThird() {
        ArrayList<String> temp = new ArrayList<>();
        temp.add(colors.get(0));
        temp.add(colors.get(2));
        return temp;
    }

    public void updateSecond() {
        colors.set(1,"Magenta color");
    }

    public void removeThird(){
        colors.remove(2);
    }

    public int searchElement (String element) {
        return colors.indexOf(element);
    }

    public void sort( ) {
        Collections.sort(colors);
    }

    public void reverse( ) {
        Collections.reverse(colors);
    }

    public void swap (String elementOne, String elementTwo) {
        int a,b;
        a=colors.indexOf(elementOne);
        b=colors.indexOf(elementTwo);
        colors.set(b, elementOne);
        colors.set(a, elementTwo);
    }

    public void replaceSecond(String element) {
        colors.set(2, element);
    }

    public void copyArrayList(ArrayList<String> newList) {
        colors.clear();
        colors.addAll(newList);
    }

    public ArrayList<String> cloneList() {
        return (ArrayList<String>) colors.clone();
    }

    public void joinArrayLists(ArrayList<String> arrayList) {
        colors.addAll(arrayList);
    }
}
