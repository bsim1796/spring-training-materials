package first;

public class ThreadHelloWord extends Thread{

    private static int threadID = 0;

    private synchronized long nextThreadID() {
        return ++threadID;
    }

    @Override
    public void run() {
        System.out.println("Hello word " + this.nextThreadID());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
