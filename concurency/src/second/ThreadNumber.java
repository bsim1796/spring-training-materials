package second;

public class ThreadNumber extends Thread{
    private static Integer threadID = 1;
    private static final int MAX = 10;
    private int reminder;
    static Object lock=new Object();

    public ThreadNumber(String name, int reminder) {
        super(name);
        this.reminder = reminder;
    }

    @Override
    public void run() {
        while (threadID < MAX-1){
            synchronized (lock) {
                while (threadID % 3 != reminder) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                System.out.println(this.getName() + "  " + threadID++);
                lock.notifyAll();
            }
        }
    }
}
