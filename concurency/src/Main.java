import first.ThreadHelloWord;
import second.ThreadNumber;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        /*
        List<ThreadHelloWord> thwList = new ArrayList<>();
        for(int i=0;i<5;i++) {
            thwList.add(new ThreadHelloWord());
        }
        thwList.stream().forEach(Thread::start);*/

        ThreadNumber threadNumber1 = new ThreadNumber("T1",1);
        ThreadNumber threadNumber2 = new ThreadNumber("T2",2);
        ThreadNumber threadNumber3 = new ThreadNumber("T3",0);
        threadNumber1.start();
        threadNumber2.start();
        threadNumber3.start();


    }
}
