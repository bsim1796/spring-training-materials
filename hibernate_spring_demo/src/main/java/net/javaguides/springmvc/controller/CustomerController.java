package net.javaguides.springmvc.controller;

import net.javaguides.springmvc.dao.CustomerDAO;
import net.javaguides.springmvc.entity.Customer;
import net.javaguides.springmvc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/list")
    public ModelAndView listCustomers() {
        List < Customer > theCustomers = customerService.getCustomers();
        ModelAndView mv = new ModelAndView();
        mv.setViewName("list-customers");
        mv.addObject("customers", theCustomers);
        return mv;
    }

    @GetMapping("/showFormForAdd")
    public ModelAndView showFormForAdd() {
        Customer theCustomer = new Customer();
        ModelAndView mv = new ModelAndView();
        mv.setViewName("customer-form");
        mv.addObject("customer", theCustomer);
        return mv;
    }

    @PostMapping("/saveCustomer")
    public String saveCustomer(@ModelAttribute("customer") Customer theCustomer) {
        customerService.saveCustomer(theCustomer);
        return "redirect:/customer/list";
    }

    @GetMapping("/showFormForUpdate")
    public ModelAndView showFormForUpdate(@RequestParam("customerId") int theId) {
        Customer theCustomer = customerService.getCustomer(theId);
        ModelAndView mv = new ModelAndView();
        mv.setViewName("customer-form");
        mv.addObject("customer", theCustomer);
        return mv;
    }

    @GetMapping("/delete")
    public String deleteCustomer(@RequestParam("customerId") int theId) {
        customerService.deleteCustomer(theId);
        return "redirect:/customer/list";
    }
}