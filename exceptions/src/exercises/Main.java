package exercises;

import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) {

        Logger logger = Logger.getLogger("MainLogger");

        /// 4. task
        String myStr = null;
        try {
            /// Or we can add an if (myStr != null) block
            System.out.println(myStr.length());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


        /**
         * 6. task
         * Catching a NullPointerException does not handle the ArithmeticException: we need to catch the correct type of exception.
         */

        try {
            int data = 25/0;
        } catch (ArithmeticException e) {
            System.out.println(e);
        } finally {
            System.out.println("Finally block");
        }
        System.out.println("Rest of the code");
    }
}
