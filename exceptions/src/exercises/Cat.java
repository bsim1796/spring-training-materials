package exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class Cat {

    /**
    First of all there was no catch block in this example, secondly, the function "cat" can throw an exception in the finally block,
     (input.close) so the keyword "throws IOException" is required in the declaration.
     Also, we could use the try-with-resource type of handler.
     */
    public static void cat (File file) throws IOException {
        RandomAccessFile input = null;
        String line = null;
        try {
            input = new RandomAccessFile(file, "r");
            while ((line = input.readLine()) != null ) {
                System.out.println(line);
            }
            return;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                input.close();
            }
        }
    }
}
