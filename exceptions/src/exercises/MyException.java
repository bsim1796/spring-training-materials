package exercises;

public class MyException extends Exception{

    public MyException() {
        super();
    }

    public MyException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return "Hello from exercises.MyException " + super.getMessage();
    }

    @Override
    public String toString() {
        return "My exception was thrown";
    }
}
