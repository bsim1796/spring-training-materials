package exceptions.levelerror;

import exceptions.BasicClass;
import exceptions.BasicLogged;
import exercises.MyException;

import java.util.logging.Logger;

public class ErrorLogged implements BasicLogged {
    private final Logger LOGGER = Logger.getLogger(this.getClass().getName());
    private static final int MAX_WEIGHT = 160;

    private int weight;
    private String message;
    private Object basicClass;

    @Override
    public int getWeight() {
        LOGGER.info(" Info log");
        LOGGER.warning("Debug log");
        LOGGER.severe("Error log \n\n");

        return weight;
    }

    @Override
    public void setWeight(int weight) {
        LOGGER.info(" Info log");
        LOGGER.warning("Debug log");
        LOGGER.severe("Error log \n\n");
        this.weight = weight;
    }

    @Override
    public String getMessages() {
        LOGGER.info(" Info log");
        LOGGER.warning("Debug log");
        LOGGER.severe("Error log \n\n");
        return message;
    }

    @Override
    public void setMessages(String messages) {
        LOGGER.info(" Info log");
        LOGGER.warning("Debug log");
        LOGGER.severe("Error log \n\n");
        this.message=messages;
    }

    @Override
    public Object getBasicClass() {
        LOGGER.info(" Info log");
        LOGGER.warning("Debug log");
        LOGGER.severe("Error log \n\n");
        return basicClass;
    }

    @Override
    public void setBasicClass(Object basicClass) {
        LOGGER.info(" Info log");
        LOGGER.warning("Debug log");
        LOGGER.severe("Error log \n\n");
        this.basicClass = basicClass;
    }

    @Override
    public boolean isOverweight() {
        LOGGER.info(" Info log");
        LOGGER.warning("Debug log");
        LOGGER.severe("Error log \n\n");
        return weight > MAX_WEIGHT;
    }

    @Override
    public void evaluateBasicClass() throws MyException {
        LOGGER.info(" Info log");
        LOGGER.warning("Debug log");
        LOGGER.severe("Error log \n\n");
        if(basicClass instanceof BasicClass) {
            throw new MyException("Basic class does not match");
        }
    }
}