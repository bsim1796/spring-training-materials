public class ID {
    private static long id=1;

    private ID() {
    }

    public static long getInstance(){
        return id++;
    }
}
