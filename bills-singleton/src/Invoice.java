import java.util.ArrayList;

public class Invoice {

    private long InvoiceID;
    private ArrayList<Product.BillableProduct> products;

    public Invoice(ArrayList<Product.BillableProduct> products) {
        this.InvoiceID = ID.getInstance();
        this.products = products;
    }
    public Invoice() {
        this.InvoiceID = ID.getInstance();
        this.products = new ArrayList<Product.BillableProduct>();
    }

    public void addProducts(Product.BillableProduct product) {
        this.products.add(product);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Invoice{");
        sb.append("InvoiceID=").append(InvoiceID);
        sb.append(", products=").append(products);
        sb.append('}');
        return sb.toString();
    }
}
