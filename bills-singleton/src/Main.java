import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Product printer = new Product("printer",Product.Quality.MEDIUM, 399);
        Product pan = new Product("pan", Product.Quality.EXCELLENT, 70);
        Product.BillableProduct pans = pan.new BillableProduct(7);
        Product.BillableProduct printers = printer.new BillableProduct(2);

        Invoice invoice1 = new Invoice();
        Invoice invoice2 = new Invoice();
        Invoice invoice3 = new Invoice();
        invoice1.addProducts(pans);
        invoice2.addProducts(printers);
        invoice3.addProducts(pans);
        invoice3.addProducts(printers);

        System.out.println(invoice1.toString());
        System.out.println(invoice2.toString());
        System.out.println(invoice3.toString());
    }
}
