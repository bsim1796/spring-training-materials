public class Product {
    private String name;
    private Quality quality;
    private int price;


    public enum Quality {
        BAD, MEDIUM, GOOD, EXCELLENT
    }

    class BillableProduct {
        private int quantity;
        private int totalPrice;

        public BillableProduct(int quantity) {
            this.quantity = quantity;
            this.totalPrice = quantity * Product.this.price;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("BillableProduct{");
            sb.append("quantity=").append(quantity);
            sb.append(", totalPrice=").append(totalPrice);
            sb.append(", Product=").append(Product.this);
            sb.append('}');
            return sb.toString();
        }
    }

    public Product(String name, Quality quality, int price) {
        this.name = name;
        this.quality = quality;
        this.price = price;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Product{");
        sb.append("name='").append(name).append('\'');
        sb.append(", quality=").append(quality);
        sb.append(", price=").append(price);
        sb.append('}');
        return sb.toString();
    }

    public String getName() {
        return name;
    }

    public Quality getQuality() {
        return quality;
    }

    public int getPrice() {
        return price;
    }
}
