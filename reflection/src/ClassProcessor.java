import java.lang.reflect.Field;
import java.util.Arrays;

public class ClassProcessor {

    public static void printFields (Class thisClass) {
        Arrays.stream(thisClass.getDeclaredFields()).forEach(System.out::println);
    }

    public static void printMethods(Class thisClass) {
        Arrays.stream(thisClass.getDeclaredMethods()).forEach(System.out::println);
    }
}
