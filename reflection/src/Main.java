public class Main {

    public static void main(String[] args) {
        ClassProcessor.printFields(String.class);
        ClassProcessor.printMethods(String.class);
    }
}
