create database if not exists atm_secure;
use atm_secure;

drop table if exists Transaction;
drop table if exists Account;
drop table if exists user_role;
drop table if exists User;
drop table if exists Role;
drop table if exists ATM;
drop table if exists Bank;


create table Bank (
	id int primary key NOT NULL AUTO_INCREMENT,
    name varchar(254) DEFAULT NULL,
    transfer_fee_percent double DEFAULT NULL
);

create table ATM (
	id int primary key NOT NULL AUTO_INCREMENT,
	address varchar(254) DEFAULT NULL,
    bank_id int,
    constraint FK_atms_banks foreign key (bank_id) references Bank(id)
);

create table User(
	id int primary key NOT NULL AUTO_INCREMENT,
	identifier varchar(254) DEFAULT NULL,
    pin_code varchar(254) DEFAULT NULL,
    name varchar(254) DEFAULT NULL,
    age int DEFAULT NULL,
    email varchar(254) DEFAULT NULL,
    active boolean DEFAULT true
);

create table Role(
	id int primary key NOT NULL AUTO_INCREMENT,
    role_name varchar(254) DEFAULT NULL
);

create table user_role(
	user_id int NOT NULL,
    role_id int NOT NULL,
    primary key(user_id, role_id),
    constraint FK_user_userrole foreign key (user_id) references User(id),
    constraint FK_role_userrole foreign key (role_id) references Role(id)
);

create table Account(
	id int primary key NOT NULL AUTO_INCREMENT,
	type varchar(254) DEFAULT NULL,
    iban varchar(254) DEFAULT NULL,
    balance double DEFAULT NULL,
    currency varchar(254) DEFAULT NULL,
    user_id int DEFAULT NULL,
    bank_id int DEFAULT NULL,
    constraint FK_acc_user foreign key (user_id) references User(id),
    constraint FK_acc_bank foreign key (bank_id) references Bank(id)
);

create table Transaction (
	id int primary key NOT NULL AUTO_INCREMENT,
    sender_account_id int DEFAULT NULL,
    destination_account_id int DEFAULT NULL,
    amount double DEFAULT NULL,
    date datetime DEFAULT NULL,
    constraint FK_sender_acc_transaction foreign key (sender_account_id) references Account(id),
    constraint FK_destination_acc_transaction foreign key (destination_account_id) references Account(id)
);







insert into Bank (name, transfer_fee_percent ) values 
("Banca Transilvania",2.1),
("OTP", 4.3),
("CEC", 3.6);

insert into Atm (address, bank_id) values
("Cluj Napoca Str Garii 12",1),
("Cluj Napoca Str Garii 13",2),
("Cluj Napoca Str Motiilor 13",1);

insert into User (identifier, pin_code, name, age, email) values 
("123454321","{bcrypt}$2a$10$g0Wd2a9dzqzwlcKlV21IlOCh/r1byqFcye4XyKFeXlQq5T3i8i.km","Harley Quinn",26,"harleybarley34@gmail.com"),
("987656789","{bcrypt}$2a$10$UFFplbVPeFnLZM.hqhJFceEH8C0k/hI57A7gE4jLeAOzGO8UU13Fu","Joker",55,"whysoserious@gmail.com"),
("987654321","{bcrypt}$2a$10$L4jn0UW2d0YD3pLFWBPCb.Lx1IfK4f5tjTwI/Bz9u0sbophfTT6Y6","Batman",40,"imbatman4sure@gmail.com");

insert into Account (type, iban, balance, currency, user_id, bank_id) values
("debit","RO1234DDDD123456",553.43,"RON",1,1),
("saving","RO1234SSSS123456",1234.56,"EURO",1,1),
("debit","RO4321DDDD123456",1234,"RON",2,2);

insert into Transaction (sender_account_id, destination_account_id, amount, date) values
(1,3,32.5,now()),
(1,3,312.5,now()),
(1,3,3.5,now()),
(3,1,99.9,now());

insert into Role (role_name) values
("ROLE_USER"),
("ROLE_ADMIN"),
("ROLE_SYSTEM");

insert into user_role (user_id, role_id) values
(1,1),
(1,2),
(2,1),
(3,1),
(3,3);

select * from user;