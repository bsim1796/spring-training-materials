package com.example.springrestatm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class SpringRestAtmApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringRestAtmApplication.class, args);
    }

}
