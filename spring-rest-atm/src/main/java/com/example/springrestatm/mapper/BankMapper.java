package com.example.springrestatm.mapper;

import com.example.springrestatm.dto.incoming.creation.BankCreationDto;
import com.example.springrestatm.dto.incoming.update.BankUpdateDto;
import com.example.springrestatm.dto.outgoing.BankDto;
import com.example.springrestatm.dto.outgoing.detailed.BankDetailsDto;
import com.example.springrestatm.model.Bank;
import org.mapstruct.*;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
@Component
public abstract class BankMapper {
    
    public abstract BankDto modelToReducedDto(Bank model);
    
    public abstract BankDetailsDto modelToDetailsDto(Bank model);
    
    public abstract Bank creationDtoToModel(BankCreationDto dto);
    
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    public abstract Bank updateModelFromDto(BankUpdateDto dto, @MappingTarget Bank answer);
    
    @IterableMapping(elementTargetType = BankDto.class)
    public abstract Iterable<BankDto> modelsToDtos(Iterable<Bank> models);

}