package com.example.springrestatm.mapper;

import com.example.springrestatm.dto.incoming.creation.TransactionCreationDto;
import com.example.springrestatm.dto.incoming.update.TransactionUpdateDto;
import com.example.springrestatm.dto.outgoing.TransactionDto;
import com.example.springrestatm.dto.outgoing.detailed.TransactionDetailsDto;
import com.example.springrestatm.model.Transaction;
import org.mapstruct.*;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
@Component
public abstract class TransactionMapper {
    public abstract TransactionDto modelToReducedDto(Transaction model);

    public abstract TransactionDetailsDto modelToDetailsDto(Transaction model);

    public abstract Transaction creationDtoToModel(TransactionCreationDto dto);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    public abstract Transaction updateModelFromDto(TransactionUpdateDto dto, @MappingTarget Transaction answer);

    @IterableMapping(elementTargetType = TransactionDto.class)
    public abstract Iterable<TransactionDto> modelsToDtos(Iterable<Transaction> models);

}
