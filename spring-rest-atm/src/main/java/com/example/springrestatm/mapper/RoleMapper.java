package com.example.springrestatm.mapper;

import com.example.springrestatm.dto.outgoing.RoleDto;
import com.example.springrestatm.dto.outgoing.detailed.RoleDetailsDto;
import com.example.springrestatm.model.Role;
import org.mapstruct.*;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
@Component
public abstract class RoleMapper {
    public abstract RoleDto modelToReducedDto(Role model);

    public abstract RoleDetailsDto modelToDetailsDto(Role model);

    @IterableMapping(elementTargetType = RoleDto.class)
    public abstract Iterable<RoleDto> modelsToDtos(Iterable<Role> models);
}
