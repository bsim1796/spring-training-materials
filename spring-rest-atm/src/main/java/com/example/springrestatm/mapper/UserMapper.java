package com.example.springrestatm.mapper;

import com.example.springrestatm.dto.incoming.creation.UserCreationDto;
import com.example.springrestatm.dto.incoming.update.UserUpdateDto;
import com.example.springrestatm.dto.outgoing.detailed.UserDetailsDto;
import com.example.springrestatm.dto.outgoing.UserDto;
import com.example.springrestatm.model.User;
import org.mapstruct.*;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
@Component
public abstract class UserMapper{

    public abstract UserDto modelToReducedDto(User model);

    public abstract UserDetailsDto modelToDetailsDto(User model);

    public abstract User creationDtoToModel(UserCreationDto dto);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    public abstract User updateModelFromDto(UserUpdateDto dto, @MappingTarget User answer);

    @IterableMapping(elementTargetType = UserDto.class)
    public abstract Iterable<UserDto> modelsToDtos(Iterable<User> models);

}
