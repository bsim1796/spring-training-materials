package com.example.springrestatm.mapper;

import com.example.springrestatm.dto.incoming.creation.ATMCreationDto;
import com.example.springrestatm.dto.incoming.update.ATMUpdateDto;
import com.example.springrestatm.dto.outgoing.ATMDto;
import com.example.springrestatm.dto.outgoing.detailed.ATMDetailsDto;
import com.example.springrestatm.model.ATM;
import org.mapstruct.*;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
@Component
public abstract class ATMMapper {
    
    public abstract ATMDto modelToReducedDto(ATM model);

    public abstract ATMDetailsDto modelToDetailsDto(ATM model);

    public abstract ATM creationDtoToModel(ATMCreationDto dto);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    public abstract ATM updateModelFromDto(ATMUpdateDto dto, @MappingTarget ATM answer);

    @IterableMapping(elementTargetType = ATMDto.class)
    public abstract Iterable<ATMDto> modelsToDtos(Iterable<ATM> models);
}
