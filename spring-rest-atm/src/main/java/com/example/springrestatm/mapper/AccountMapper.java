package com.example.springrestatm.mapper;

import com.example.springrestatm.dto.incoming.creation.AccountCreationDto;
import com.example.springrestatm.dto.incoming.update.AccountUpdateDto;
import com.example.springrestatm.dto.outgoing.AccountDto;
import com.example.springrestatm.dto.outgoing.detailed.AccountDetailsDto;
import com.example.springrestatm.model.Account;
import org.mapstruct.*;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
@Component
public abstract class AccountMapper {

    @Mapping(target = "currency",
            expression = "java(model.getCurrency().name())")
    public abstract AccountDto modelToReducedDto(Account model);

    @Mapping(target = "currency",
            expression = "java(model.getCurrency().name())")
    public abstract AccountDetailsDto modelToDetailsDto(Account model);

    @Mapping(target = "currency",
            expression = "java(com.example.springrestatm.utils.Currency.valueOf(dto.getCurrency()))")
    public abstract Account creationDtoToModel(AccountCreationDto dto);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "currency",
            expression = "java(com.example.springrestatm.utils.Currency.valueOf(dto.getCurrency()))")
    public abstract Account updateModelFromDto(AccountUpdateDto dto, @MappingTarget Account answer);

    @IterableMapping(elementTargetType = AccountDto.class)
    public abstract Iterable<AccountDto> modelsToDtos(Iterable<Account> models);
}
