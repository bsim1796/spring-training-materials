package com.example.springrestatm.exception;

import com.example.springrestatm.model.BaseEntity;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class NotFoundException extends RuntimeException {

    final Class<? extends BaseEntity> entityClass;
    final Integer id;

    @Override
    public String getMessage() {
        return "Could not find entity of type " + entityClass.getSimpleName() + " with ID " + id;
    }
}
