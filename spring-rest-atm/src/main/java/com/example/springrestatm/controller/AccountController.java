package com.example.springrestatm.controller;

import com.example.springrestatm.dto.incoming.creation.AccountCreationDto;
import com.example.springrestatm.dto.incoming.update.AccountUpdateDto;
import com.example.springrestatm.exception.NotFoundException;
import com.example.springrestatm.facade.impl.AccountDefaultFacadeImpl;
import com.example.springrestatm.facade.AccountFacade;
import com.example.springrestatm.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.Serializable;

@Controller
@RequestMapping("/api/accounts")
@Slf4j
public class AccountController {

    @Autowired
    AccountFacade facade = new AccountDefaultFacadeImpl();

    @GetMapping()
    @ResponseBody
    public ResponseEntity<?> findAllAccounts () {
        try{
            return ResponseEntity.status(HttpStatus.OK).body(facade.findAllAccounts());
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("findAccount failed...");
        }
    }

    @GetMapping("/{id}")
    @ResponseBody
    public ResponseEntity<? extends Serializable> findAccountById(@PathVariable("id") Integer id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(facade.findAccountById(id));
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("findAccountById failed...");
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PostMapping()
    @ResponseBody
    public ResponseEntity<? extends Serializable> createAccount(@RequestBody @Valid AccountCreationDto accountDto) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(facade.createAccount(accountDto));
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("createAccount failed...");
        }
    }

    @PutMapping("/{id}")
    @ResponseBody
    public ResponseEntity<? extends Serializable> patchAccount(@PathVariable("id") Integer id, @RequestBody @Valid AccountUpdateDto accountDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(facade.putAccount(id, accountDto));
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("createAccount failed...");
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<? extends Serializable> deleteAccount(@PathVariable("id") Integer id) {
        try {
            facade.deleteAccount(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Account with id= " + id + " deleted");
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("deleteAccount failed...");
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }
}
