package com.example.springrestatm.controller;

import com.example.springrestatm.dto.incoming.creation.UserCreationDto;
import com.example.springrestatm.dto.incoming.update.UserUpdateDto;
import com.example.springrestatm.exception.NotFoundException;
import com.example.springrestatm.facade.impl.UserDefaultFacadeImpl;
import com.example.springrestatm.facade.UserFacade;
import com.example.springrestatm.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.Serializable;

@Controller
@RequestMapping("/api/users")
@Slf4j
public class UserController {

    @Autowired
    UserFacade facade = new UserDefaultFacadeImpl();

    @GetMapping()
    @ResponseBody
    public ResponseEntity<?> findAllUsers () {
        try{
            return ResponseEntity.status(HttpStatus.OK).body(facade.findAllUsers());
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("findUser failed...");
        }
    }

    @GetMapping("/{id}")
    @ResponseBody
    public ResponseEntity<? extends Serializable> findUserById(@PathVariable("id") Integer id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(facade.findUserById(id));
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("findUserById failed...");
        }
        catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PostMapping()
    @ResponseBody
    public ResponseEntity<? extends Serializable> createUser(@RequestBody @Valid UserCreationDto userDto) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(facade.createUser(userDto));
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("createUser failed...");
        }
    }

    @PutMapping("/{id}")
    @ResponseBody
    public ResponseEntity<? extends Serializable> patchUser(@PathVariable("id") Integer id, @RequestBody @Valid UserUpdateDto userDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(facade.putUser(id, userDto));
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("createUser failed...");
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<? extends Serializable> deleteUser(@PathVariable("id") Integer id) {
        try {
            facade.deleteUser(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("User with id= " + id + " deleted");

        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("deleteUser failed...");
        }
        catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

}
