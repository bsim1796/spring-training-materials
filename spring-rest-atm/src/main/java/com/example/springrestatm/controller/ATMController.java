package com.example.springrestatm.controller;

import com.example.springrestatm.dto.incoming.creation.ATMCreationDto;
import com.example.springrestatm.dto.incoming.update.ATMUpdateDto;
import com.example.springrestatm.exception.NotFoundException;
import com.example.springrestatm.facade.impl.ATMDefaultFacadeImpl;
import com.example.springrestatm.facade.ATMFacade;
import com.example.springrestatm.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.Serializable;

@Controller
@RequestMapping("/api/atms")
@Slf4j
public class ATMController {

    @Autowired
    ATMFacade facade = new ATMDefaultFacadeImpl();

    @GetMapping()
    @ResponseBody
    public ResponseEntity<?> findAllATMs () {
        try{
            return ResponseEntity.status(HttpStatus.OK).body(facade.findAllATMs());
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("findATM failed...");
        }
    }

    @GetMapping("/{id}")
    @ResponseBody
    public ResponseEntity<? extends Serializable> findATMById(@PathVariable("id") Integer id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(facade.findATMById(id));
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("findATMById failed...");
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PostMapping()
    @ResponseBody
    public ResponseEntity<? extends Serializable> createATM(@RequestBody @Valid ATMCreationDto ATMDto) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(facade.createATM(ATMDto));
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("createATM failed...");
        }
    }

    @PutMapping("/{id}")
    @ResponseBody
    public ResponseEntity<? extends Serializable> patchATM(@PathVariable("id") Integer id, @RequestBody @Valid ATMUpdateDto ATMDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(facade.putATM(id, ATMDto));
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("createATM failed...");
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<? extends Serializable> deleteATM(@PathVariable("id") Integer id) {
        try {
            facade.deleteATM(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("ATM with id= " + id + " deleted");
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("deleteATM failed...");
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }
}
