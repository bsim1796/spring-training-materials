package com.example.springrestatm.controller;

import com.example.springrestatm.dto.incoming.creation.BankCreationDto;
import com.example.springrestatm.dto.incoming.update.BankUpdateDto;
import com.example.springrestatm.exception.NotFoundException;
import com.example.springrestatm.facade.impl.BankDefaultFacadeImpl;
import com.example.springrestatm.facade.BankFacade;
import com.example.springrestatm.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.Serializable;

@Controller
@RequestMapping("/api/banks")
@Slf4j
public class BankController {

    @Autowired
    BankFacade facade = new BankDefaultFacadeImpl();

    @GetMapping()
    @ResponseBody
    public ResponseEntity<?> findAllBanks () {
        try{
            return ResponseEntity.status(HttpStatus.OK).body(facade.findAllBanks());
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("findBank failed...");
        }
    }

    @GetMapping("/{id}")
    @ResponseBody
    public ResponseEntity<? extends Serializable> findBankById(@PathVariable("id") Integer id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(facade.findBankById(id));
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("findBankById failed...");
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PostMapping()
    @ResponseBody
    public ResponseEntity<? extends Serializable> createBank(@RequestBody @Valid BankCreationDto bankDto) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(facade.createBank(bankDto));
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("createBank failed...");
        }
    }

    @PutMapping("/{id}")
    @ResponseBody
    public ResponseEntity<? extends Serializable> patchBank(@PathVariable("id") Integer id, @RequestBody @Valid BankUpdateDto bankDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(facade.putBank(id, bankDto));
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("createBank failed...");
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<? extends Serializable> deleteBank(@PathVariable("id") Integer id) {
        try {
            facade.deleteBank(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Bank with id= " + id + " deleted");
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("deleteBank failed...");
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }
}
