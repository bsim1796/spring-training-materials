package com.example.springrestatm.controller;

import com.example.springrestatm.dto.incoming.creation.TransactionCreationDto;
import com.example.springrestatm.dto.incoming.update.TransactionUpdateDto;
import com.example.springrestatm.exception.NotFoundException;
import com.example.springrestatm.facade.impl.TransactionDefaultFacadeImpl;
import com.example.springrestatm.facade.TransactionFacade;
import com.example.springrestatm.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.Serializable;

@Controller
@RequestMapping("/api/transactions")
@Slf4j
public class TransactionController {

    @Autowired
    TransactionFacade facade = new TransactionDefaultFacadeImpl();

    @GetMapping()
    @ResponseBody
    public ResponseEntity<?> findAllTransactions () {
        try{
            return ResponseEntity.status(HttpStatus.OK).body(facade.findAllTransactions());
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("findTransaction failed...");
        }
    }

    @GetMapping("/{id}")
    @ResponseBody
    public ResponseEntity<? extends Serializable> findTransactionById(@PathVariable("id") Integer id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(facade.findTransactionById(id));
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("findTransactionById failed...");
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PostMapping()
    @ResponseBody
    public ResponseEntity<? extends Serializable> createTransaction(@RequestBody @Valid TransactionCreationDto transactionDto) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(facade.createTransaction(transactionDto));
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("createTransaction failed...");
        }
    }

    @PutMapping("/{id}")
    @ResponseBody
    public ResponseEntity<? extends Serializable> patchTransaction(@PathVariable("id") Integer id, @RequestBody @Valid TransactionUpdateDto transactionDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(facade.putTransaction(id, transactionDto));
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("createTransaction failed...");
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<? extends Serializable> deleteTransaction(@PathVariable("id") Integer id) {
        try {
            facade.deleteTransaction(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Transaction with id= " + id + " deleted");

        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("deleteTransaction failed...");
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }
}
