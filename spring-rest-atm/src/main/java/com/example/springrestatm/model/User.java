package com.example.springrestatm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class User extends BaseEntity implements Serializable {
    @Column
    private String identifier;
    @Column
    private String pin_code;
    @Column
    private String name;
    @Column
    private Integer age;
    @Column
    private String email;
    @Column
    private boolean active;

    @ManyToMany(targetEntity = Role.class, fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    @JsonIgnoreProperties("users")
    private Collection<Role> roles = new ArrayList<>();

    @OneToMany(targetEntity = Account.class, cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"user", "bank"})
    private Collection<Account> accounts = new ArrayList<>();
}

