package com.example.springrestatm.model;

import com.example.springrestatm.utils.Currency;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Account extends BaseEntity implements Serializable {

    @Column
    String type;
    @Column
    String iban;
    @Column
    Double balance;
    @Enumerated(EnumType.STRING)
    @Column
    Currency currency;

    @ManyToOne(targetEntity = User.class,fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @JsonIgnoreProperties("accounts")
    private User user;

    @ManyToOne(targetEntity = Bank.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "bank_id")
    @JsonIgnoreProperties({"accounts", "atms"})
    private Bank bank;
}
