package com.example.springrestatm.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Transaction extends BaseEntity implements Serializable {
    @Column
    Double amount;
    @Column
    Date date;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sender_account_id")
    Account sender;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "destination_account_id")
    Account destination;
}
