package com.example.springrestatm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Bank extends BaseEntity implements Serializable {

    @Column
    String name;
    @Column
    Double transfer_fee_percent;

    @OneToMany(targetEntity = ATM.class,cascade = CascadeType.ALL, mappedBy = "bank", fetch = FetchType.LAZY)
    @JsonIgnoreProperties("bank")
    private Collection<ATM> atms = new ArrayList<>();

    @OneToMany(targetEntity = Account.class, cascade = CascadeType.ALL, mappedBy = "bank", fetch = FetchType.LAZY)
    @JsonIgnoreProperties("bank")
    private Collection<Account> accounts = new ArrayList<>();
}
