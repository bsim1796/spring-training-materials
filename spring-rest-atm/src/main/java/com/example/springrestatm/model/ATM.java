package com.example.springrestatm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ATM extends BaseEntity implements Serializable {
    @Column
    private String address;

    @ManyToOne(targetEntity = Bank.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "bank_id")
    @JsonIgnoreProperties({"atms","accounts"})
    Bank bank;
}
