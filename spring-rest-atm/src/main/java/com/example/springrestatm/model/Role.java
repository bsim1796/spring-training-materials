package com.example.springrestatm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Role extends BaseEntity{
    @Column
    private String role_name;

    @ManyToMany(targetEntity = User.class, mappedBy = "roles", cascade = {CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @JsonIgnoreProperties("roles")
    private Collection<User> users = new ArrayList<>();
}
