package com.example.springrestatm.facade;

import com.example.springrestatm.dto.incoming.creation.AccountCreationDto;
import com.example.springrestatm.dto.incoming.update.AccountUpdateDto;
import com.example.springrestatm.dto.outgoing.AccountDto;
import com.example.springrestatm.dto.outgoing.detailed.AccountDetailsDto;

public interface AccountFacade {
    public Iterable<AccountDto> findAllAccounts ();

    public AccountDetailsDto findAccountById(Integer id);

    public AccountDetailsDto createAccount(AccountCreationDto AccountDto);

    public AccountDetailsDto putAccount(Integer id, AccountUpdateDto AccountDto);

    public void deleteAccount(Integer id);
}
