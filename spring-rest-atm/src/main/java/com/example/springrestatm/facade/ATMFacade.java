package com.example.springrestatm.facade;

import com.example.springrestatm.dto.incoming.creation.ATMCreationDto;
import com.example.springrestatm.dto.incoming.update.ATMUpdateDto;
import com.example.springrestatm.dto.outgoing.ATMDto;
import com.example.springrestatm.dto.outgoing.detailed.ATMDetailsDto;

public interface ATMFacade {
    public Iterable<ATMDto> findAllATMs ();

    public ATMDetailsDto findATMById(Integer id);

    public ATMDetailsDto createATM(ATMCreationDto ATMDto);

    public ATMDetailsDto putATM(Integer id, ATMUpdateDto ATMDto);

    public void deleteATM(Integer id);
}
