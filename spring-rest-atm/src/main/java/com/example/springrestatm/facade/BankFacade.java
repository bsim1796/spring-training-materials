package com.example.springrestatm.facade;

import com.example.springrestatm.dto.incoming.creation.BankCreationDto;
import com.example.springrestatm.dto.incoming.update.BankUpdateDto;
import com.example.springrestatm.dto.outgoing.BankDto;
import com.example.springrestatm.dto.outgoing.detailed.BankDetailsDto;

public interface BankFacade {
    public Iterable<BankDto> findAllBanks ();

    public BankDetailsDto findBankById(Integer id);

    public BankDetailsDto createBank(BankCreationDto BankDto);

    public BankDetailsDto putBank(Integer id, BankUpdateDto BankDto);

    public void deleteBank(Integer id);
}
