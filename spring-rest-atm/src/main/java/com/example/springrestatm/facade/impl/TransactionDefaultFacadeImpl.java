package com.example.springrestatm.facade.impl;

import com.example.springrestatm.exception.NotFoundException;
import com.example.springrestatm.dto.incoming.creation.TransactionCreationDto;
import com.example.springrestatm.dto.incoming.update.TransactionUpdateDto;
import com.example.springrestatm.dto.outgoing.detailed.TransactionDetailsDto;
import com.example.springrestatm.dto.outgoing.TransactionDto;
import com.example.springrestatm.facade.TransactionFacade;
import com.example.springrestatm.mapper.TransactionMapper;
import com.example.springrestatm.model.Transaction;
import com.example.springrestatm.service.impl.TransactionDefaultServiceImpl;
import com.example.springrestatm.service.TransactionService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
public class TransactionDefaultFacadeImpl implements TransactionFacade {

    @Autowired
    private TransactionService TransactionService = new TransactionDefaultServiceImpl();

    @Autowired
    private TransactionMapper mapper;

    @Override
    public Iterable<TransactionDto> findAllTransactions() {
        List<Transaction> Transactions = (List<Transaction>) TransactionService.findAllTransaction();
        return mapper.modelsToDtos(Transactions);
    }

    @Override
    public TransactionDetailsDto findTransactionById(Integer id) {
        Transaction Transaction = TransactionService.findTransactionById(id);
        if (Transaction == null) {
            throw new NotFoundException(Transaction.class, id);
        }
        return mapper.modelToDetailsDto(Transaction);
    }

    @Override
    public TransactionDetailsDto createTransaction(TransactionCreationDto TransactionDto) {
        Transaction Transaction = mapper.creationDtoToModel(TransactionDto);
        Transaction = TransactionService.saveTransaction(Transaction);
        return mapper.modelToDetailsDto(Transaction);
    }


    @Override
    public TransactionDetailsDto putTransaction(Integer id, TransactionUpdateDto TransactionDto) {
        Transaction Transaction = TransactionService.findTransactionById(id);
        if (Transaction == null) {
            throw new NotFoundException(Transaction.class, id);
        }
        Transaction = mapper.updateModelFromDto(TransactionDto, Transaction);
        Transaction = TransactionService.saveTransaction(Transaction);
        return mapper.modelToDetailsDto(Transaction);
    }

    @Override
    public void deleteTransaction(Integer id) {
        if (!TransactionService.transactionExists(id)) {
            throw new NotFoundException(Transaction.class, id);
        }
        TransactionService.deleteTransaction(id);
    }
}
