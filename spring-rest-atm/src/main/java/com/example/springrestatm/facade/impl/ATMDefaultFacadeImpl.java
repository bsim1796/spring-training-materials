package com.example.springrestatm.facade.impl;

import com.example.springrestatm.exception.NotFoundException;
import com.example.springrestatm.dto.incoming.creation.ATMCreationDto;
import com.example.springrestatm.dto.incoming.update.ATMUpdateDto;
import com.example.springrestatm.dto.outgoing.detailed.ATMDetailsDto;
import com.example.springrestatm.dto.outgoing.ATMDto;
import com.example.springrestatm.facade.ATMFacade;
import com.example.springrestatm.mapper.ATMMapper;
import com.example.springrestatm.model.ATM;
import com.example.springrestatm.service.impl.ATMDefaultServiceImpl;
import com.example.springrestatm.service.ATMService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
public class ATMDefaultFacadeImpl implements ATMFacade {

    @Autowired
    private ATMService ATMService = new ATMDefaultServiceImpl();

    @Autowired
    private ATMMapper mapper;

    @Override
    public Iterable<ATMDto> findAllATMs() {
        List<ATM> ATMs = (List<ATM>) ATMService.findAllATM();
        return mapper.modelsToDtos(ATMs);
    }

    @Override
    public ATMDetailsDto findATMById(Integer id) {
        ATM ATM = ATMService.findATMById(id);
        if (ATM == null) {
            throw new NotFoundException(ATM.class, id);
        }
        return mapper.modelToDetailsDto(ATM);
    }

    @Override
    public ATMDetailsDto createATM(ATMCreationDto ATMDto) {
        ATM ATM = mapper.creationDtoToModel(ATMDto);
        ATM = ATMService.saveATM(ATM);
        return mapper.modelToDetailsDto(ATM);
    }


    @Override
    public ATMDetailsDto putATM(Integer id, ATMUpdateDto ATMDto) {
        ATM ATM = ATMService.findATMById(id);
        if (ATM == null) {
            throw new NotFoundException(ATM.class, id);
        }
        ATM = mapper.updateModelFromDto(ATMDto, ATM);
        ATM = ATMService.saveATM(ATM);
        return mapper.modelToDetailsDto(ATM);
    }

    @Override
    public void deleteATM(Integer id) {
        if (!ATMService.ATMExists(id)) {
            throw new NotFoundException(ATM.class, id);
        }
        ATMService.deleteATM(id);
    }
}
