package com.example.springrestatm.facade;

import com.example.springrestatm.dto.incoming.creation.UserCreationDto;
import com.example.springrestatm.dto.incoming.update.UserUpdateDto;
import com.example.springrestatm.dto.outgoing.detailed.UserDetailsDto;
import com.example.springrestatm.dto.outgoing.UserDto;

public interface UserFacade {
    public Iterable<UserDto> findAllUsers ();

    public UserDetailsDto findUserById(Integer id);

    public UserDetailsDto createUser(UserCreationDto userDto);

    public UserDetailsDto putUser(Integer id, UserUpdateDto userDto);

    public void deleteUser(Integer id);

}
