package com.example.springrestatm.facade.impl;

import com.example.springrestatm.exception.NotFoundException;
import com.example.springrestatm.dto.incoming.creation.BankCreationDto;
import com.example.springrestatm.dto.incoming.update.BankUpdateDto;
import com.example.springrestatm.dto.outgoing.detailed.BankDetailsDto;
import com.example.springrestatm.dto.outgoing.BankDto;
import com.example.springrestatm.facade.BankFacade;
import com.example.springrestatm.mapper.BankMapper;
import com.example.springrestatm.model.Bank;
import com.example.springrestatm.service.impl.BankDefaultServiceImpl;
import com.example.springrestatm.service.BankService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
public class BankDefaultFacadeImpl implements BankFacade {

    @Autowired
    private BankService BankService = new BankDefaultServiceImpl();

    @Autowired
    private BankMapper mapper;

    @Override
    public Iterable<BankDto> findAllBanks() {
        List<Bank> Banks = (List<Bank>) BankService.findAllBank();
        return mapper.modelsToDtos(Banks);
    }

    @Override
    public BankDetailsDto findBankById(Integer id) {
        Bank Bank = BankService.findBankById(id);
        if (Bank == null) {
            throw new NotFoundException(Bank.class, id);
        }
        return mapper.modelToDetailsDto(Bank);
    }

    @Override
    public BankDetailsDto createBank(BankCreationDto BankDto) {
        Bank Bank = mapper.creationDtoToModel(BankDto);
        Bank = BankService.saveBank(Bank);
        return mapper.modelToDetailsDto(Bank);
    }


    @Override
    public BankDetailsDto putBank(Integer id, BankUpdateDto BankDto) {
        Bank Bank = BankService.findBankById(id);
        if (Bank == null) {
            throw new NotFoundException(Bank.class, id);
        }
        Bank = mapper.updateModelFromDto(BankDto, Bank);
        Bank = BankService.saveBank(Bank);
        return mapper.modelToDetailsDto(Bank);
    }

    @Override
    public void deleteBank(Integer id) {
        if (!BankService.bankExists(id)) {
            throw new NotFoundException(Bank.class, id);
        }
        BankService.deleteBank(id);
    }
}
