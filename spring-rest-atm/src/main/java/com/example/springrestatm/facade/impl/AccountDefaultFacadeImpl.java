package com.example.springrestatm.facade.impl;

import com.example.springrestatm.exception.NotFoundException;
import com.example.springrestatm.dto.incoming.creation.AccountCreationDto;
import com.example.springrestatm.dto.incoming.update.AccountUpdateDto;
import com.example.springrestatm.dto.outgoing.detailed.AccountDetailsDto;
import com.example.springrestatm.dto.outgoing.AccountDto;
import com.example.springrestatm.facade.AccountFacade;
import com.example.springrestatm.mapper.AccountMapper;
import com.example.springrestatm.model.Account;
import com.example.springrestatm.service.impl.AccountDefaultServiceImpl;
import com.example.springrestatm.service.AccountService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
public class AccountDefaultFacadeImpl implements AccountFacade {

    @Autowired
    private AccountService accountService = new AccountDefaultServiceImpl();

    @Autowired
    private AccountMapper mapper;

    @Override
    public Iterable<AccountDto> findAllAccounts() {
        List<Account> Accounts = (List<Account>) accountService.findAllAccount();
        return mapper.modelsToDtos(Accounts);
    }

    @Override
    public AccountDetailsDto findAccountById(Integer id) {
        Account Account = accountService.findAccountById(id);
        if (Account == null) {
            throw new NotFoundException(Account.class, id);
        }
        return mapper.modelToDetailsDto(Account);
    }

    @Override
    public AccountDetailsDto createAccount(AccountCreationDto accountDto) {
        Account Account = mapper.creationDtoToModel(accountDto);
        Account = accountService.saveAccount(Account);
        return mapper.modelToDetailsDto(Account);
    }


    @Override
    public AccountDetailsDto putAccount(Integer id, AccountUpdateDto accountDto) {
        Account Account = accountService.findAccountById(id);
        if (Account == null) {
            throw new NotFoundException(Account.class, id);
        }
        Account = mapper.updateModelFromDto(accountDto, Account);
        Account = accountService.saveAccount(Account);
        return mapper.modelToDetailsDto(Account);
    }

    @Override
    public void deleteAccount(Integer id) {
        if (!accountService.accountExists(id)) {
            throw new NotFoundException(Account.class, id);
        }
        accountService.deleteAccount(id);
    }
}
