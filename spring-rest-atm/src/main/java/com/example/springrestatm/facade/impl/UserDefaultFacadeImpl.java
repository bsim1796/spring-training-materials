package com.example.springrestatm.facade.impl;

import com.example.springrestatm.exception.NotFoundException;
import com.example.springrestatm.dto.incoming.creation.UserCreationDto;
import com.example.springrestatm.dto.incoming.update.UserUpdateDto;
import com.example.springrestatm.dto.outgoing.detailed.UserDetailsDto;
import com.example.springrestatm.dto.outgoing.UserDto;
import com.example.springrestatm.facade.UserFacade;
import com.example.springrestatm.mapper.UserMapper;
import com.example.springrestatm.model.User;
import com.example.springrestatm.service.impl.UserDefaultServiceImpl;
import com.example.springrestatm.service.UserService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
public class UserDefaultFacadeImpl implements UserFacade {

    @Autowired
    private UserService userService = new UserDefaultServiceImpl();

    @Autowired
    private UserMapper mapper;

    @Override
    public Iterable<UserDto> findAllUsers() {
        List<User> users = (List<User>) userService.findAllUser();
        return mapper.modelsToDtos(users);
    }

    @Override
    public UserDetailsDto findUserById(Integer id) {
        User user = userService.findUserById(id);
        if (user == null) {
            throw new NotFoundException(User.class, id);
        }
        return mapper.modelToDetailsDto(user);
    }

    @Override
    public UserDetailsDto createUser(UserCreationDto userDto) {
        User user = mapper.creationDtoToModel(userDto);
        user = userService.saveUser(user);
        return mapper.modelToDetailsDto(user);
    }


    @Override
    public UserDetailsDto putUser(Integer id, UserUpdateDto userDto) {
        User user = userService.findUserById(id);
        if (user == null) {
            throw new NotFoundException(User.class, id);
        }
        user = mapper.updateModelFromDto(userDto, user);
        user = userService.saveUser(user);
        return mapper.modelToDetailsDto(user);
    }

    @Override
    public void deleteUser(Integer id) {
        if (!userService.userExists(id)) {
            throw new NotFoundException(User.class, id);
        }
        userService.deleteUser(id);
    }
}
