package com.example.springrestatm.facade;

import com.example.springrestatm.dto.incoming.creation.TransactionCreationDto;
import com.example.springrestatm.dto.incoming.update.TransactionUpdateDto;
import com.example.springrestatm.dto.outgoing.TransactionDto;
import com.example.springrestatm.dto.outgoing.detailed.TransactionDetailsDto;

public interface TransactionFacade {
    public Iterable<TransactionDto> findAllTransactions ();

    public TransactionDetailsDto findTransactionById(Integer id);

    public TransactionDetailsDto createTransaction(TransactionCreationDto TransactionDto);

    public TransactionDetailsDto putTransaction(Integer id, TransactionUpdateDto TransactionDto);

    public void deleteTransaction(Integer id);
}
