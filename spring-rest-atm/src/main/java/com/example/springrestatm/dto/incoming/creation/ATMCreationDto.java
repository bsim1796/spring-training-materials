package com.example.springrestatm.dto.incoming.creation;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class ATMCreationDto implements Serializable {
    @NotEmpty
    @Size(max = 254)
    private String address;
}
