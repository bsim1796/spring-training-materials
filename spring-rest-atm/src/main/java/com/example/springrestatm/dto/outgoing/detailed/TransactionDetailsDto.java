package com.example.springrestatm.dto.outgoing.detailed;

import com.example.springrestatm.dto.outgoing.AccountDto;
import com.example.springrestatm.dto.outgoing.TransactionDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class TransactionDetailsDto extends TransactionDto {
    private AccountDto sender;
    private AccountDto destination;
}
