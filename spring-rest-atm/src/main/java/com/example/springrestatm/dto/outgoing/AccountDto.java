package com.example.springrestatm.dto.outgoing;

import lombok.Data;

import java.io.Serializable;

@Data
public class AccountDto implements Serializable {
    private Integer id;
    private String type;
    private String iban;
    private Double balance;
    private String currency;
}
