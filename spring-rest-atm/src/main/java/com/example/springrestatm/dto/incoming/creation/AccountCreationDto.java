package com.example.springrestatm.dto.incoming.creation;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class AccountCreationDto implements Serializable {
    @NotEmpty
    @Size(max = 254)
    private String type;
    @NotEmpty
    @Size(max = 254)
    private String iban;
    @NotEmpty
    private Double balance;
    @Size(max = 254)
    @NotEmpty
    private String currency;
}
