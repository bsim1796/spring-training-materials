package com.example.springrestatm.dto.incoming.update;

import lombok.Data;

import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;
import java.io.Serializable;
import java.sql.Date;

@Data
public class TransactionUpdateDto implements Serializable {
    @Positive
    private Double amount;
    @PastOrPresent
    private Date date;
}
