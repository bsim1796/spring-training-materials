package com.example.springrestatm.dto.outgoing;

import lombok.Data;

import java.io.Serializable;

@Data
public class BankDto implements Serializable {
    private Integer id;
    private String name;
    private Double transfer_fee_percent;
}
