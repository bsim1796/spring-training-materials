package com.example.springrestatm.dto.outgoing;

import lombok.Data;

import java.io.Serializable;

@Data
public class ATMDto implements Serializable {
    private Integer id;
    private String address;
}
