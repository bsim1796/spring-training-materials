package com.example.springrestatm.dto.outgoing;

import lombok.Data;

import java.io.Serializable;
import java.sql.Date;

@Data
public class TransactionDto implements Serializable {
    private Integer id;
    private Double amount;
    private Date date;
}
