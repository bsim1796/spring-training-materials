package com.example.springrestatm.dto.incoming.update;

import lombok.Data;

import javax.validation.constraints.*;
import java.io.Serializable;

@Data
public class UserUpdateDto implements Serializable {
    private String identifier;
    @Pattern(regexp="[\\d]{6}")
    private Integer pin_code;
    @Size(max = 254)
    private String name;
    @Positive
    private Integer age;
    @Email
    @Size(max = 254)
    private String email;
    private Boolean active;
}
