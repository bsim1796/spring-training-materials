package com.example.springrestatm.dto.incoming.creation;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class BankCreationDto implements Serializable {
    @NotEmpty
    @Size(max = 254)
    private String name;
    @NotEmpty
    @Positive
    private Double transfer_fee_percent;
}
