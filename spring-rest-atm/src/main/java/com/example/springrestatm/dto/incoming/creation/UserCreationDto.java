package com.example.springrestatm.dto.incoming.creation;

import lombok.Data;

import javax.validation.constraints.*;
import java.io.Serializable;

@Data
public class UserCreationDto implements Serializable {
    @NotEmpty
    private String identifier;
    @Pattern(regexp="[\\d]{4}")
    private String pin_code;
    @NotEmpty
    @Size(max = 254)
    private String name;
    @NotEmpty
    @Positive
    private Integer age;
    @NotEmpty
    @Email
    @Size(max = 254)
    private String email;
    private Boolean active;
}
