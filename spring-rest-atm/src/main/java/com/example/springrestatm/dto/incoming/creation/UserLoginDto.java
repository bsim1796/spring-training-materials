package com.example.springrestatm.dto.incoming.creation;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Data
public class UserLoginDto implements Serializable {
    @NotEmpty
    private String identifier;
    @Pattern(regexp="[\\d]{4}")
    private String pin_code;
}
