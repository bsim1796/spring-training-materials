package com.example.springrestatm.dto.incoming.update;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class BankUpdateDto implements Serializable {
    @Size(max = 254)
    private String name;
    @NotEmpty
    @Positive
    private Double transfer_fee_percent;
}
