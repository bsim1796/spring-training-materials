package com.example.springrestatm.dto.outgoing;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class RoleDto {
    @NotEmpty
    @Size(max = 254)
    private String role_name;
}
