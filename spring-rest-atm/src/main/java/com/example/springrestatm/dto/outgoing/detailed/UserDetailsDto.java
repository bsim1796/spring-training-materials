package com.example.springrestatm.dto.outgoing.detailed;

import com.example.springrestatm.dto.outgoing.AccountDto;
import com.example.springrestatm.dto.outgoing.RoleDto;
import com.example.springrestatm.dto.outgoing.UserDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class UserDetailsDto extends UserDto {
    private List<AccountDto> accounts;
    private List<RoleDto> roles;
}
