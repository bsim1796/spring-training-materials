package com.example.springrestatm.dto.incoming.update;

import lombok.Data;

import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class ATMUpdateDto implements Serializable {
    @Size(max = 254)
    private String address;
}
