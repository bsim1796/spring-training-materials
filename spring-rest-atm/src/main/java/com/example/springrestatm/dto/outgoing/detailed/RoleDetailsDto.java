package com.example.springrestatm.dto.outgoing.detailed;

import com.example.springrestatm.dto.outgoing.RoleDto;
import com.example.springrestatm.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class RoleDetailsDto extends RoleDto {
    List<User> users;
}
