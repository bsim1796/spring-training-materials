package com.example.springrestatm.dto.incoming.update;

import lombok.Data;

import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class AccountUpdateDto implements Serializable {
    @Size(max = 254)
    private String type;
    @Size(max = 254)
    private String iban;
    private Double balance;
    @Size(max = 254)
    private String currency;
}
