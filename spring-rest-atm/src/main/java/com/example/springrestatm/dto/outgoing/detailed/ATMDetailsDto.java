package com.example.springrestatm.dto.outgoing.detailed;

import com.example.springrestatm.dto.outgoing.ATMDto;
import com.example.springrestatm.dto.outgoing.BankDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ATMDetailsDto extends ATMDto {
    private BankDto bank;
}
