package com.example.springrestatm.dto.outgoing.detailed;

import com.example.springrestatm.dto.outgoing.AccountDto;
import com.example.springrestatm.dto.outgoing.BankDto;
import com.example.springrestatm.dto.outgoing.UserDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class AccountDetailsDto extends AccountDto {
    private UserDto user;
    private BankDto bank;
}
