package com.example.springrestatm.dto.incoming.creation;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;
import java.io.Serializable;
import java.sql.Date;

@Data
public class TransactionCreationDto implements Serializable {
    @NotEmpty
    @Positive
    private Double amount;
    @NotEmpty
    @PastOrPresent
    private Date date;
}
