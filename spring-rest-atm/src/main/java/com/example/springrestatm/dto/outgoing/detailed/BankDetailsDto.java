package com.example.springrestatm.dto.outgoing.detailed;

import com.example.springrestatm.dto.outgoing.ATMDto;
import com.example.springrestatm.dto.outgoing.AccountDto;
import com.example.springrestatm.dto.outgoing.BankDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class BankDetailsDto extends BankDto {
    List<ATMDto> atms;
    List<AccountDto> accounts;
}
