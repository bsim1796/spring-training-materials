package com.example.springrestatm.dto.outgoing;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserDto implements Serializable {
    private Integer id;
    private String identifier;
    private String pin_code;
    private String name;
    private Integer age;
    private String email;
    private Boolean active;
}
