package com.example.springrestatm.service.impl;

import com.example.springrestatm.model.Transaction;
import com.example.springrestatm.repository.TransactionRepo;
import com.example.springrestatm.exception.ServiceException;
import com.example.springrestatm.service.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
@Slf4j
public class TransactionDefaultServiceImpl implements TransactionService {

    @Autowired
    private TransactionRepo TransactionRepo;

    @Override
    public Collection<Transaction> findAllTransaction() {
        try {
            return TransactionRepo.findAll();
        } catch (DataAccessException e) {
            log.error("findAllTransaction failed");
            throw new ServiceException("findAllTransaction failed ",e);
        }
    }

    @Override
    public Transaction findTransactionById(Integer id) {
        try {
            Optional<Transaction> optional = TransactionRepo.findById(id);
            return optional.orElse(null);
        } catch (DataAccessException e) {
            log.error("findTransactionById failed");
            throw new ServiceException("findTransactionById failed ",e);
        }
    }

    @Override
    public boolean transactionExists(Integer id) {
        try {
            return TransactionRepo.existsById(id);
        } catch (IllegalArgumentException e) {
            log.error("TransactionExists failed", e);
            throw new ServiceException("TransactionExists failed", e);
        }
    }

    @Override
    public Transaction saveTransaction(Transaction Transaction) {
        try {
            return TransactionRepo.saveAndFlush(Transaction);
        } catch (DataIntegrityViolationException e) {
            log.error("saveTransaction failed", e);
            throw new ServiceException("saveTransaction failed", e);
        }
    }

    @Override
    public void deleteTransaction(Integer id) {
        try {
            TransactionRepo.deleteById(id);
        } catch (IllegalArgumentException e) {
            log.error("deleteBook failed", e);
            throw new ServiceException("deleteBook failed", e);
        }
    }
}
