package com.example.springrestatm.service;

import com.example.springrestatm.model.Account;

import java.util.Collection;

public interface AccountService {
    public Collection<Account> findAllAccount();

    public Account findAccountById(Integer id);

    public boolean accountExists(Integer id);

    public Account saveAccount(Account accountDto);

    public void deleteAccount(Integer id);
}
