package com.example.springrestatm.service.impl;

import com.example.springrestatm.model.Bank;
import com.example.springrestatm.repository.BankRepo;
import com.example.springrestatm.service.BankService;
import com.example.springrestatm.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
@Slf4j
public class BankDefaultServiceImpl implements BankService {

    @Autowired
    private BankRepo BankRepo;

    @Override
    public Collection<Bank> findAllBank() {
        try {
            return BankRepo.findAll();
        } catch (DataAccessException e) {
            log.error("findAllBank failed");
            throw new ServiceException("findAllBank failed ",e);
        }
    }

    @Override
    public Bank findBankById(Integer id) {
        try {
            Optional<Bank> optional = BankRepo.findById(id);
            return optional.orElse(null);
        } catch (DataAccessException e) {
            log.error("findBankById failed");
            throw new ServiceException("findBankById failed ",e);
        }
    }

    @Override
    public boolean bankExists(Integer id) {
        try {
            return BankRepo.existsById(id);
        } catch (IllegalArgumentException e) {
            log.error("BankExists failed", e);
            throw new ServiceException("BankExists failed", e);
        }
    }

    @Override
    public Bank saveBank(Bank Bank) {
        try {
            return BankRepo.saveAndFlush(Bank);
        } catch (DataIntegrityViolationException e) {
            log.error("saveBank failed", e);
            throw new ServiceException("saveBank failed", e);
        }
    }

    @Override
    public void deleteBank(Integer id) {
        try {
            BankRepo.deleteById(id);
        } catch (IllegalArgumentException e) {
            log.error("deleteBook failed", e);
            throw new ServiceException("deleteBook failed", e);
        }
    }
}
