package com.example.springrestatm.service;

import com.example.springrestatm.model.ATM;

import java.util.Collection;

public interface ATMService {
    public Collection<ATM> findAllATM();

    public ATM findATMById(Integer id);

    public boolean ATMExists(Integer id);

    public ATM saveATM(ATM ATM);

    public void deleteATM(Integer id);
}
