package com.example.springrestatm.service.impl;

import com.example.springrestatm.model.Account;
import com.example.springrestatm.repository.AccountRepo;
import com.example.springrestatm.service.AccountService;
import com.example.springrestatm.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
@Slf4j
public class AccountDefaultServiceImpl implements AccountService {

    @Autowired
    private AccountRepo accountRepo;

    @Override
    public Collection<Account> findAllAccount() {
        try {
            return accountRepo.findAll();
        } catch (DataAccessException e) {
            log.error("findAllAccount failed");
            throw new ServiceException("findAllAccount failed ",e);
        }
    }

    @Override
    public Account findAccountById(Integer id) {
        try {
            Optional<Account> optional = accountRepo.findById(id);
            return optional.orElse(null);
        } catch (DataAccessException e) {
            log.error("findAccountById failed");
            throw new ServiceException("findAccountById failed ",e);
        }
    }

    @Override
    public boolean accountExists(Integer id) {
        try {
            return accountRepo.existsById(id);
        } catch (IllegalArgumentException e) {
            log.error("AccountExists failed", e);
            throw new ServiceException("AccountExists failed", e);
        }
    }

    @Override
    public Account saveAccount(Account account) {
        try {
            return accountRepo.saveAndFlush(account);
        } catch (DataIntegrityViolationException e) {
            log.error("saveAccount failed", e);
            throw new ServiceException("saveAccount failed", e);
        }
    }

    @Override
    public void deleteAccount(Integer id) {
        try {
            accountRepo.deleteById(id);
        } catch (IllegalArgumentException e) {
            log.error("deleteBook failed", e);
            throw new ServiceException("deleteBook failed", e);
        }
    }
}
