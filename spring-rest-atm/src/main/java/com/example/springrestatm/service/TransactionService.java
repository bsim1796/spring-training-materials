package com.example.springrestatm.service;

import com.example.springrestatm.model.Transaction;

import java.util.Collection;

public interface TransactionService {
    public Collection<Transaction> findAllTransaction();

    public Transaction findTransactionById(Integer id);

    public boolean transactionExists(Integer id);

    public Transaction saveTransaction(Transaction Transaction);

    public void deleteTransaction(Integer id);
}
