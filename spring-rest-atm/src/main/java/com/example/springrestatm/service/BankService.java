package com.example.springrestatm.service;

import com.example.springrestatm.model.Bank;

import java.util.Collection;

public interface BankService {
    public Collection<Bank> findAllBank();

    public Bank findBankById(Integer id);

    public boolean bankExists(Integer id);

    public Bank saveBank(Bank Bank);

    public void deleteBank(Integer id);
}
