package com.example.springrestatm.service;

import com.example.springrestatm.model.User;

import java.util.Collection;

public interface UserService {
    public Collection<User> findAllUser();

    public User findUserById(Integer id);

    public boolean userExists(Integer id);

    public User saveUser(User user);

    public void deleteUser(Integer id);
}
