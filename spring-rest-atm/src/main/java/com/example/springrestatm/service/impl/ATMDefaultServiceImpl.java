package com.example.springrestatm.service.impl;

import com.example.springrestatm.model.ATM;
import com.example.springrestatm.repository.ATMRepo;
import com.example.springrestatm.service.ATMService;
import com.example.springrestatm.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
@Slf4j
public class ATMDefaultServiceImpl implements ATMService {

    @Autowired
    private ATMRepo ATMRepo;

    @Override
    public Collection<ATM> findAllATM() {
        try {
            return ATMRepo.findAll();
        } catch (DataAccessException e) {
            log.error("findAllATM failed");
            throw new ServiceException("findAllATM failed ",e);
        }
    }

    @Override
    public ATM findATMById(Integer id) {
        try {
            Optional<ATM> optional = ATMRepo.findById(id);
            return optional.orElse(null);
        } catch (DataAccessException e) {
            log.error("findATMById failed");
            throw new ServiceException("findATMById failed ",e);
        }
    }

    @Override
    public boolean ATMExists(Integer id) {
        try {
            return ATMRepo.existsById(id);
        } catch (IllegalArgumentException e) {
            log.error("ATMExists failed", e);
            throw new ServiceException("ATMExists failed", e);
        }
    }

    @Override
    public ATM saveATM(ATM ATM) {
        try {
            return ATMRepo.saveAndFlush(ATM);
        } catch (DataIntegrityViolationException e) {
            log.error("saveATM failed", e);
            throw new ServiceException("saveATM failed", e);
        }
    }

    @Override
    public void deleteATM(Integer id) {
        try {
            ATMRepo.deleteById(id);
        } catch (IllegalArgumentException e) {
            log.error("deleteBook failed", e);
            throw new ServiceException("deleteBook failed", e);
        }
    }
}
