package com.example.springrestatm.service.impl;

import com.example.springrestatm.model.MyUserDetails;
import com.example.springrestatm.model.User;
import com.example.springrestatm.repository.UserRepo;
import com.example.springrestatm.exception.ServiceException;
import com.example.springrestatm.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
@Slf4j
public class UserDefaultServiceImpl implements UserService, UserDetailsService {

    @Autowired
    private UserRepo userRepo;

    @Override
    public Collection<User> findAllUser() {
        try {
            return userRepo.findAll();
        } catch (DataAccessException e) {
            log.error("findAllUser failed");
            throw new ServiceException("findAllUser failed ",e);
        }
    }

    @Override
    public User findUserById(Integer id) {
        try {
            Optional<User> optional = userRepo.findById(id);
            return optional.orElse(null);
        } catch (DataAccessException e) {
            log.error("findUserById failed");
            throw new ServiceException("findUserById failed ",e);
        }
    }

    @Override
    public boolean userExists(Integer id) {
        try {
            return userRepo.existsById(id);
        } catch (IllegalArgumentException e) {
            log.error("userExists failed", e);
            throw new ServiceException("userExists failed", e);
        }
    }

    @Override
    public User saveUser(User user) {
        try {
            return userRepo.saveAndFlush(user);
        } catch (DataIntegrityViolationException e) {
            log.error("saveUser failed", e);
            throw new ServiceException("saveUser failed", e);
        }
    }

    @Override
    public void deleteUser(Integer id) {
        try {
            userRepo.deleteById(id);
        } catch (IllegalArgumentException e) {
            log.error("deleteBook failed", e);
            throw new ServiceException("deleteBook failed", e);
        }
    }

    @Override
    public UserDetails loadUserByUsername(String identifier) throws UsernameNotFoundException {

        Optional<User> user = userRepo.findByIdentifier(identifier);

        user.orElseThrow(() -> new UsernameNotFoundException("Not found: " + identifier));

        return user.map(MyUserDetails::new).get();
    }
}
