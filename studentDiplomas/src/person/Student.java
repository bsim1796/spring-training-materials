package person;

import diploma.DiplomaForStudent;

import java.util.ArrayList;

public interface Student extends Person {
    void addDiploma(DiplomaForStudent diplomaForStudent) ;

    ArrayList<DiplomaForStudent> getDiplomas ();
}
