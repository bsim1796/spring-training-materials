package person;

public interface Person {
    String getFirstName();

    String getLastName();

    String getAddress();

    int getAge();
}
