package person.impl;

import diploma.DiplomaForStudent;
import person.Person;
import person.Student;

import java.util.ArrayList;

public class StudentImpl extends PersonImpl implements Student {

    private int studentNumber;
    private ArrayList<DiplomaForStudent> diplomaForStudents;

    public StudentImpl(Person person, int studentNumber, ArrayList<DiplomaForStudent> diplomaForStudents) {
        super(person);
        this.studentNumber = studentNumber;
        this.diplomaForStudents = diplomaForStudents;
    }



    public void addDiploma(DiplomaForStudent diplomaForStudent) {
        this.diplomaForStudents.add(diplomaForStudent);
    }

    public ArrayList<DiplomaForStudent> getDiplomas () {
        return diplomaForStudents;
    }
}
