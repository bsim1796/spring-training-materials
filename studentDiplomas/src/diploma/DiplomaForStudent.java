package diploma;

import i18n.Locales;

public interface DiplomaForStudent {
    String getDiplomas();

    void setTitle(String title);

    String toString ();

    String toString (Locales.Language language);
}
