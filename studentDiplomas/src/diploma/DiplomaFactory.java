package diploma;

import diploma.impl.BasicDiploma;
import diploma.impl.Doctor;
import diploma.impl.Licence;
import diploma.impl.Master;

public class DiplomaFactory {

    public DiplomaForStudent createDiploma (){
        return new BasicDiploma();
    }

    public DiplomaForStudent createDiploma (BasicDiploma.DiplomaType diplomaType){
        switch (diplomaType) {
            case LICENCE:
                return new Licence();
            case MASTER:
                return new Master();
            case PHD:
                return new Doctor();
            default:
                return new BasicDiploma();
        }
    }
}
