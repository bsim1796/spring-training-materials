package diploma.impl;

import i18n.Locales;

public class Master extends BasicDiploma {

    private static final String COMPUTER_SCIENCE = "Computer science";

    public Master() {
        super(COMPUTER_SCIENCE, DiplomaType.MASTER);
    }

    @Override
    public String toString() {
        return Locales.English.MASTER;
    }

    public String toString(Locales.Language language) {
        return language.getMasterM();
    }

}
