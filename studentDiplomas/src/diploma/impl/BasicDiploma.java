package diploma.impl;

import diploma.DiplomaForStudent;
import i18n.Locales;

public class BasicDiploma implements DiplomaForStudent {

    private String title;
    private DiplomaType diplomaType;

    private static final String LICENCE = "Licence";
    private static final String MASTER = "Master";
    private static final String PHD = "Ph. D.";
    private static final String BASIC_DIPLOMA = "basic diploma";


    public enum DiplomaType {
        LICENCE(BasicDiploma.LICENCE,4,16),
        MASTER(BasicDiploma.MASTER, 2,18),
        PHD(BasicDiploma.PHD,3,20);

        private final String title;
        private final int durationOfStudy;
        private final int admissionAge;

        DiplomaType(String title, int durationOfStudy, int admissionAge) {
            this.title = title;
            this.durationOfStudy = durationOfStudy;
            this.admissionAge = admissionAge;
        }

        public String getTitle() {
            return title;
        }

        public Integer getDurationOfStudy() {
            return durationOfStudy;
        }

        public Integer getAdmissionAge() {
            return admissionAge;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Diploma of ");
            sb.append(this.getTitle()).append(".\nDuration of study (min.): ").append(this.getDurationOfStudy());
            sb.append(".\nAdmission age (min.): ").append(this.getAdmissionAge());
            return sb.toString();
        }
    }

    public BasicDiploma(String title, DiplomaType diplomaType) {
        this.title = title;
        this.diplomaType = diplomaType;
    }

    public BasicDiploma() {
        this.title = BASIC_DIPLOMA;
        this.diplomaType = null;
    }

    public String getDiplomas() {
        return this.toString();
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public DiplomaType getDiplomaType() {
        return diplomaType;
    }

    @Override
    public String toString() {
        return Locales.English.DEFAULT;
    }


    public String toString(Locales.Language language) {
        return language.getDefaultM();
    }
}
