package diploma.impl;

import i18n.Locales;

public class Doctor extends BasicDiploma {

    private static final String COMPUTER_SCIENCE = "Computer science";

    public Doctor() {
        super(COMPUTER_SCIENCE, DiplomaType.PHD);
    }

    @Override
    public String toString() {
        return Locales.English.DOCTOR;
    }

    public String toString(Locales.Language language) {
        return language.getDoctorM();
    }
}
