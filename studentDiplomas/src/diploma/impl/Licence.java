package diploma.impl;

import i18n.Locales;

public class Licence extends BasicDiploma {

    private static final String COMPUTER_SCIENCE = "Computer science";

    public Licence() {
        super(COMPUTER_SCIENCE, DiplomaType.LICENCE);
    }

    @Override
    public String toString() {
        return Locales.English.LICENCE;
    }

    public String toString(Locales.Language language) {
        return language.getLicenceM();
    }
}
