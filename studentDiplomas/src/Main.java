import diploma.DiplomaFactory;
import diploma.DiplomaForStudent;
import diploma.impl.BasicDiploma;
import i18n.Locales;
import person.Person;
import person.Student;
import person.impl.PersonImpl;
import person.impl.StudentImpl;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Person person = new PersonImpl("Jobs", "Steve", "1st street, 1", 32);
        Student student = new StudentImpl(person,12345, new ArrayList<DiplomaForStudent>());
        DiplomaFactory diplomaFactory = new DiplomaFactory();

        DiplomaForStudent license = diplomaFactory.createDiploma(BasicDiploma.DiplomaType.LICENCE);
        DiplomaForStudent master = diplomaFactory.createDiploma(BasicDiploma.DiplomaType.MASTER);
        DiplomaForStudent doctor = diplomaFactory.createDiploma(BasicDiploma.DiplomaType.PHD);



        student.addDiploma(license);
        student.addDiploma(master);
        student.addDiploma(doctor);

        student.getDiplomas().forEach((n) -> System.out.println(n.toString(Locales.Language.SPANISH)));

        DiplomaForStudent basicDiploma = diplomaFactory.createDiploma();
        System.out.println(basicDiploma.toString());
    }
}
