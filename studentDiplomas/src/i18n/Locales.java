package i18n;

public class Locales {

    public static class English {
        public static final String DEFAULT = "Enrolled";
        public static final String LICENCE = "Enrolled for Licence";
        public static final String MASTER = "Enrolled for Master";
        public static final String DOCTOR = "Enrolled for Ph. D.";
    }

    public static class Spanish {
        public static final String DEFAULT = "Inscrita";
        public static final String LICENCE = "Inscrita para la licencia";
        public static final String MASTER = "Inscrita para la Masteria";
        public static final String DOCTOR = "Inscrita para la Ph. D.";
    }

    public static class France {
        public static final String DEFAULT = "Inscrite";
        public static final String LICENCE = "Inscrit pour la licence";
        public static final String MASTER = "Inscrit pour la Master";
        public static final String DOCTOR = "Inscrit pour la Ph. D.";
    }

    public static class Slovak {
        public static final String DEFAULT = "Zapisany";
        public static final String LICENCE = "Zapisany pre licenciu";
        public static final String MASTER = "Zapisany pre Masteru";
        public static final String DOCTOR = "Zapisany pre Ph. D.";
    }

    public static class Hungarian {
        public static final String DEFAULT = "Beiratkozott";
        public static final String LICENCE = "Beiratkozott a licencre";
        public static final String MASTER = "Beiratkozott a Masterre";
        public static final String DOCTOR = "Beiratkozott a Ph. D.-re";
    }

    public static class Romanian {
        public static final String DEFAULT = "Inscris";
        public static final String LICENCE = "Înscris pentru licență";
        public static final String MASTER = "Înscris pentru Master";
        public static final String DOCTOR = "Înscris pentru Ph. D.";
    }



    public enum Language {
        ENGLISH(English.DEFAULT, English.LICENCE,English.MASTER,English.DOCTOR),
        HUNGARIAN(Hungarian.DEFAULT, Hungarian.LICENCE,Hungarian.MASTER,Hungarian.DOCTOR),
        ROMANIAN(Romanian.DEFAULT, Romanian.LICENCE,Romanian.MASTER,Romanian.DOCTOR),
        SPANISH(Spanish.DEFAULT, Spanish.LICENCE,Spanish.MASTER,Spanish.DOCTOR),
        FRANCE(France.DEFAULT, France.LICENCE,France.MASTER,France.DOCTOR),
        SLOVAK(Slovak.DEFAULT, Slovak.LICENCE,Slovak.MASTER,Slovak.DOCTOR);

        private final String defaultMessage;
        private final String licenceMessage;
        private final String masterMessage;
        private final String doctorMessage;

        Language(String defaultMessage, String licenceMessage, String masterMessage, String doctorMessage) {
            this.defaultMessage = defaultMessage;
            this.licenceMessage = licenceMessage;
            this.masterMessage = masterMessage;
            this.doctorMessage = doctorMessage;
        }

        public String getDefaultM() {
            return defaultMessage;
        }

        public String getLicenceM() {
            return licenceMessage;
        }

        public String getMasterM() {
            return masterMessage;
        }

        public String getDoctorM() {
            return doctorMessage;
        }
    }


}
