package edu.spring.training;

import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class WishWellFortuneService implements FortuneService{

    private List<String> fortunes;
    private File theFile = new File("E:/ANT/learn-samuel-budai/04_Ioc_Annotations/src/main/resources/messages.txt");
    private Random myRandom = new Random();

    @PostConstruct
    public void init() {
        System.out.println("Reading fortunes from file: " + theFile);
        System.out.println("File exists: " + theFile.exists());
        try (BufferedReader br = new BufferedReader(
                new FileReader(theFile))) {

            String tempLine;

            while ((tempLine = br.readLine()) != null) {
                fortunes.add(tempLine);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public WishWellFortuneService() {
        fortunes = new ArrayList<>();
    }

    @Override
    public String getDailyFortune() {
        return fortunes.get(myRandom.nextInt(fortunes.size()));
    }
}
