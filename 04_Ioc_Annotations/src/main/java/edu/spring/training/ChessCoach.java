package edu.spring.training;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ChessCoach implements Coach{
    @Autowired
    private FortuneService fortune;

    public ChessCoach() {
    }

    @Override
    public String getDailyWorkout() {
        return "Win 50 games against your mom";
    }

    @Override
    public String getDailyFortune() {
        return fortune.getDailyFortune();
    }


}
