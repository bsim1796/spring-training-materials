package edu.spring.training;

public interface FortuneService {
    public String getDailyFortune();
}
